const chokidar = require('chokidar');
const Haml = require('haml');
const path = require('path');
const fs = require('fs');
const recursive = require("recursive-readdir");
const sass = require("node-sass");
const webpack = require("webpack");
const process = require("process");
const { exec } = require('child_process');

const args = process.argv.slice(2);
const RELEASE = args.length && args[0].includes("r");

// SOURCE
const JAVASCRIPT_ENTRY_POINT = path.resolve(__dirname, 'javascript/index.js');
const PYTHON_ENTRY_POINT = path.resolve(__dirname, "run.py");
const HTML_ENTRY_POINT = path.resolve(__dirname, 'html/index.html');
const HAML_DIR = path.resolve(__dirname, "haml");
const SASS_DIR = path.resolve(__dirname, "sass");
const IMAGES_DIR = path.resolve(__dirname, "images");

// BUILD
const BUILD_DIR = path.resolve(__dirname, "build");
const PYINSTALLER_DIR = path.resolve(BUILD_DIR, "pyinstaller");
const RELEASE_DIR = path.resolve(BUILD_DIR, "release");
const ASSETS_DIR = RELEASE ? path.resolve(RELEASE_DIR, "static") : path.resolve(BUILD_DIR, "sandbox/static");
const INDEX_OUT = path.resolve(ASSETS_DIR, "index.html");
const CSS_OUT = path.resolve(ASSETS_DIR, "index.css");

if(!fs.existsSync(ASSETS_DIR)) {
    fs.mkdirSync(ASSETS_DIR, { recursive: true });
}

function getHtmlTemplate(hamlPath) {
    let haml = fs.readFileSync(hamlPath, "utf8");
    let html = Haml.render(haml);
    return `<template id='template__${path.basename(hamlPath, ".haml")}'>${html}</template>`;
}

function generateIndex(hamlFiles) {
    let templateStr = hamlFiles.filter((hamlFile) => hamlFile.endsWith(".haml")).map(getHtmlTemplate).join("\n");
    let indexTemplate = fs.readFileSync(HTML_ENTRY_POINT, "utf8");
    let index = indexTemplate.replace("{{TEMPLATES}}", templateStr);
    fs.writeFileSync(INDEX_OUT, index, "utf8");
}

function generateCss(sassFiles) {
    let sharedStr = "";
    let otherStr = "";
    sassFiles.forEach((sassPath) => {
        if(sassPath.endsWith(".scss")) {
            if(sassPath.includes("shared")) {
                sharedStr += fs.readFileSync(sassPath, "utf8");
            }
            else {
                otherStr += fs.readFileSync(sassPath, "utf8");
            }
        }
    });
    let result = sass.renderSync({
        data: sharedStr + otherStr
    });
    fs.writeFileSync(CSS_OUT, result.css, "utf8");
}

function moveImages(imageFiles) {
    imageFiles.forEach(function(imageFile) {
        fs.copyFileSync(imageFile, path.join(ASSETS_DIR, path.basename(imageFile)));
    });
}

function hamlTask() {
    console.log("Building templates...");
    recursive(HAML_DIR).then(generateIndex);
}

function sassTask() {
    console.log("Building style...");
    recursive(SASS_DIR).then(generateCss);
}

function imagesTask() {
    console.log("Moving images...");
    recursive(IMAGES_DIR).then(moveImages);
}

const webpackConfig = {
    mode: RELEASE ? 'production' : 'development',
    devtool: 'eval-source-map',
    entry: {
        index: JAVASCRIPT_ENTRY_POINT
    },
    output: {
        path: ASSETS_DIR,
        filename: '[name].bun.js'
    }
};

function developmentBuild() {
    chokidar.watch(["haml", "html"]).on("change", hamlTask);
    chokidar.watch("sass").on("change", sassTask);
    const imagesWatcher = chokidar.watch("images").on("ready", () => {
        imagesWatcher.on("add", imagesTask).on("change", imagesTask);
    });

    hamlTask();
    sassTask();
    imagesTask();
    webpack(webpackConfig).watch({}, function(error, stats) {
        console.log("Webpacking...");
    });
}

function releaseBuild() {
    hamlTask();
    sassTask();
    imagesTask();
    webpack(webpackConfig).run(function() {
        console.log("Webpacked");
    });
    exec(`pyinstaller -y -F --workpath ${PYINSTALLER_DIR} --specpath ${PYINSTALLER_DIR} --distpath ${RELEASE_DIR} -n LamuneTranslate.exe ${PYTHON_ENTRY_POINT}`, function(err, stdout, stderr) {
        console.log(err || "Built server");
    });
}


if(RELEASE) {
    releaseBuild();
}
else {
    developmentBuild();
}

//pyinstaller -y -F  "C:/Users/Matt/Documents/Python/lamune-translate-unlimited-frame-works/run.py"
