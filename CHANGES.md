### v1.1.0
 - Upgraded CanvasJS
 - Refactoring of code structure to be reused on the website
 - Modified CanvasJS to play nice with NextJS
 - Added total comment counter next to each entry
 - Fixes to edge label rendering in graph
 - Allow jumping to the name definition for any spoken line for easy adjustments
 - Spoken/narration lines now use an auto-resizing textarea so long lines are fully visible
 - Refactored stats graph tooltip on "File progress by tag" to sort by tag with colors

### v1.0.0
 - Initial release
