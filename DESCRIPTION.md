A graphical tool for translating Lamune into another language, with many ways to track progress.

### Features
 - Lookup any highlighted term on popular dictionaries/translation tools
 - Progress bars and statistics everywhere
 - Graph view to visualize the branches between scripts, as well as the conditions and rewards they give
 - Statistics page of the translation progress, with different views for translated/finalized lines
 - Can add comments and resolve them to any line
 - Search any text translated or original and jump to that location
 - Search for files, with a list of the most recent visited available
 - Mark lines as complete after editing them
 - Quickly build and run the scripts to try them out in game

### Depends on
 - LamuneExtract.exe
 - lamune-hack.exe