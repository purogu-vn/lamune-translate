from twisted.web import server, resource
from twisted.internet import reactor

from python.data.io import read_static
from python.routes.counts import CountRoute
from python.routes.execute import ExecuteRoute
from python.routes.graph import GraphRoute
from python.routes.script import ScriptRoute
from python.routes.request import Request
from python.routes.search import SearchRoute
from python.routes.tags import TagRoute

ROUTES = [ScriptRoute, TagRoute, CountRoute, SearchRoute, GraphRoute, ExecuteRoute]
MIME_TYPES = {
	".css": "text/css",
	".js": "application/javascript",
	".ico": "image/x-icon",
	".html": "text/html",
	".png": "image/png",
	".svg": "image/svg+xml"
}
CACHE_EXTENSIONS = [".ico", ".png", ".svg"]
class Server(resource.Resource):
	def __init__(self, port):
		super().__init__()
		self.port = port
		for route in ROUTES:
			self.putChild(route.PATH.encode('utf8'), route())

	def getChild(self, path, r_request):
		return self

	def render_GET(self, r_request):
		return self.serve_file(Request(r_request))

	def serve_file(self, request):
		file_name = request.current_path()
		if not file_name:
			file_name = "index.html"
		raw = read_static(file_name)
		if raw:
			ext = file_name[file_name.rfind('.'):]
			if ext in MIME_TYPES:
				request.set_content_type(MIME_TYPES[ext])
				if ext in CACHE_EXTENSIONS:
					request.add_header("cache-control", "public, max-age=31536000")
				return raw
			else:
				raise ValueError("Unknown extension: {}".format(ext))
		else:
			raise ValueError("No file found: {}".format(file_name))

	def start(self):
		site = server.Site(self)
		reactor.listenTCP(self.port, site)
		reactor.run()
