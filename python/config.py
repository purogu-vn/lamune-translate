from configparser import RawConfigParser
from pathlib import Path

DEFAULTS = {
	"GameFolder": "game",
	"ScriptFolder": "scripts",
	"ExtractorExe": "LamuneExtract.exe",
	"ServerPort": "8080",
	"ReleaseFolder": "release",
	"WebFolder": "static",
	"DataFolder": "data"
}

config = RawConfigParser()
config.optionxform = str
FAKE_SECTION = "fake"

cfg_file = Path("lamune.cfg")
if not cfg_file.exists():
	print("WARNING: No config file found.")

with open("lamune.cfg", "r", encoding='utf8') as f:
	config.read_string("[{}]\n{}".format(FAKE_SECTION, f.read()))

config_dict = config[FAKE_SECTION]
for key, val in DEFAULTS.items():
	if key not in config_dict:
		print("No value given for {}, using default of {}".format(key, val))
		config_dict[key] = val
for key, val in config[FAKE_SECTION].items():
	globals()[key] = val