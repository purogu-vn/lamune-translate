from python.data.io import list_all_scripts, read_script, read_names, NAMES_FILE
from python.data.master import LamuneData
from python.data.script import ScriptSaverProcessor
from python.routes.route import Route

class ScriptRoute(Route):
	PATH = "scripts"

	def routemap(self):
		return {
			"list": self.script_list,
			"content": self.script_content,
			"names": self.retrieve_names,
			"save": self.save_script,
			"history": self.get_history,
			"recent": self.most_recent_script,
		}

	def script_list(self, request):
		file_list = list_all_scripts()
		return request.json_response(file_list)

	def script_content(self, request):
		file_name = request.get_arg("file_name")
		content = read_script(file_name)
		LamuneData.history_data.push_history(file_name)
		return request.json_response(content)

	def retrieve_names(self, request):
		return request.json_response(read_names())

	def save_script(self, request):
		changes = request.body()
		print(changes)
		for file_name, file_changes in changes.items():
			if "lines" in file_changes:
				print(file_changes["lines"])
				saver = ScriptSaverProcessor(file_name, file_changes["lines"])
				saver.run(file_changes["lastLine"] if "lastLine" in file_changes else None)
		LamuneData.graph_data.commit()
		return request.json_response(LamuneData.count_data.counts_by_file_summary())

	def get_history(self, request):
		return request.json_response(LamuneData.history_data.get_history())

	def most_recent_script(self, request):
		history = LamuneData.history_data.get_history()
		most_recent = history[-1] if len(history) > 0 else NAMES_FILE
		return request.json_response(most_recent)
