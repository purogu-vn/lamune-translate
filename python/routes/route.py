from twisted.web import resource

from python.routes.request import Request

class Route(resource.Resource):
	isLeaf = True

	def routemap(self):
		return {}

	def render(self, r_request):
		request = Request(r_request)
		subcommand = request.post_path()
		routes = self.routemap()
		if subcommand in routes:
			return routes[subcommand](request)
		else:
			raise ValueError("{} is not a valid subpath of {}".format(subcommand, self.PATH))