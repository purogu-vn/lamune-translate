import json


class Request:
	def __init__(self, request):
		self.request = request

	def current_path(self):
		return self.request.prepath[-1].decode('utf8')

	def post_path(self):
		return self.request.postpath[0].decode('utf8') if len(self.request.postpath) else None

	def set_content_type(self, type):
		self.add_header("content-type", type)

	def add_header(self, name, value):
		self.request.responseHeaders.addRawHeader(name.encode('utf8'), value.encode('utf8'))

	def json_response(self, j):
		self.set_content_type("application/json")
		return json.dumps(j).encode('utf8')

	def ok_response(self):
		return b""


	def get_arg(self, key):
		return self.request.args[key.encode('utf8')][0].decode('utf8')

	def body(self):
		return json.loads(self.request.content.read().decode('utf8'))