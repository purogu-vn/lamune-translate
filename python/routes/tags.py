from python.data.master import LamuneData
from python.data.tags import TagData
from python.routes.route import Route


class TagRoute(Route):
	PATH = "tags"

	def routemap(self):
		return {
			None: self.get_tags,
			"for_files": self.tags_for_files
		}

	def get_tags(self, request):
		return request.json_response(TagData.TAGS)

	def tags_for_files(self, request):
		return request.json_response(LamuneData.tag_data.all_tags_for_files())