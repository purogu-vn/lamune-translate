from python.data.master import LamuneData
from python.routes.route import Route


class SearchRoute(Route):
	PATH = "search"

	def routemap(self):
		return {
			None: self.search,
			"status": self.search_status
		}

	def search(self, request):
		term = request.get_arg("term")
		page = int(request.get_arg("page"))
		filter_tl = request.get_arg("tl") == "t"
		filter_finished = request.get_arg("finished") == "t"
		return request.json_response(LamuneData.indexer.search(term, page, filter_tl, filter_finished))

	def search_status(self, request):
		return request.json_response(LamuneData.indexer.pending_actions())
