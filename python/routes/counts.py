from python.data.master import LamuneData
from python.routes.route import Route


class CountRoute(Route):
	PATH = "counts"

	def routemap(self):
		return {
			"for_files_summary": self.summary_for_files,
			"for_tags": self.for_tags,
			"for_files": self.for_files,
			"for_day_then_tag": self.for_day_then_tag
		}

	def summary_for_files(self, request):
		summary = LamuneData.count_data.counts_by_file_summary()
		return request.json_response(summary)

	def for_tags(self, request):
		counts = LamuneData.count_data.counts_by_tag()
		return request.json_response(counts)

	def for_files(self, request):
		counts = LamuneData.count_data.counts_by_file()
		return request.json_response(counts)

	def for_day_then_tag(self, request):
		counts = LamuneData.count_data.counts_by_day_then_tag()
		return request.json_response(counts)