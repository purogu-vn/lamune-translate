from python.data.master import LamuneData
from python.routes.route import Route

class ExecuteRoute(Route):
	PATH = "execute"

	def routemap(self):
		return {
			"build": self.start_build,
			"build_status": self.build_status,
			"run": self.start_game,
			"release": self.generate_release
		}

	def start_build(self, request):
		LamuneData.executor.schedule_build()
		return request.ok_response()

	def build_status(self, request):
		building = LamuneData.executor.is_busy()
		return request.json_response(1 if building else 0)

	def start_game(self, request):
		LamuneData.executor.run_game()
		return request.ok_response()

	def generate_release(self, request):
		name = request.get_arg("name")
		message = LamuneData.executor.generate_release(name)
		return request.json_response(message)