from python.data.master import LamuneData
from python.routes.route import Route


class GraphRoute(Route):
	PATH = "graph"

	def routemap(self):
		return {
			None: self.get_graph
		}

	def get_graph(self, request):
		return request.json_response(LamuneData.graph_data.get_graph())