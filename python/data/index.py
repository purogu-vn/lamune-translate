from whoosh.fields import SchemaClass, ID, STORED, NGRAM, NGRAMWORDS, BOOLEAN
from whoosh import index
from whoosh.qparser import SimpleParser, PlusMinusPlugin, MultifieldPlugin
from whoosh.query import Term, And

from queue import Queue, Empty
from threading import Thread

class Schema(SchemaClass):
	id = ID(unique=True, stored=True)
	file = ID(stored=True)
	lineNum = STORED()
	original = NGRAM(stored=True)
	tl = NGRAMWORDS(stored=True)
	has_tl = BOOLEAN()
	is_finished = BOOLEAN()

class ScriptIndexer(Thread):
	def __init__(self, directory, batch_size):
		Thread.__init__(self)
		self.directory = directory
		self.batch_size = batch_size
		self.queue = Queue()
		self.daemon = True
		self.existing = index.exists_in(self.directory)
		self.index = self.get_or_create_index()
		self.parser = self.create_parser()

	def get_or_create_index(self):
		if self.existing:
			print("Loading index")
			return index.open_dir(self.directory)
		else:
			print("Initializing index")
			self.directory.mkdir()
			return index.create_in(self.directory, Schema)

	def create_parser(self):
		parser = SimpleParser("tl", schema=self.index.schema)
		parser.add_plugin(MultifieldPlugin(["original", "tl"]))
		parser.remove_plugin_class(PlusMinusPlugin)
		return parser

	def id_for(self, file, line_num, choice_num):
		if choice_num is not None:
			return "{}-{}-{}".format(file, line_num, choice_num)
		else:
			return "{}-{}".format(file, line_num)

	def queue_add(self, command, file_name, original, tl, line_num, finished):
		if not self.existing:
			self.queue_action(command, file_name, original, tl, line_num, finished, True)

	def queue_update(self, command, file_name, original, tl, line_num, finished):
		self.queue_action(command, file_name, original, tl, line_num, finished, False)

	def queue_action(self, command, file_name, original, tl, line_num, finished, new):
		if command == "choice":
			for choice_num, (coriginal, ctl) in enumerate(zip(original, tl)):
				self.queue_one(command, file_name, coriginal, ctl, line_num, choice_num, finished, new)
		else:
			self.queue_one(command, file_name, original, tl, line_num, None, finished, new)


	def queue_one(self, command, file_name, original, tl, line_num, choice_num, finished, new):
		id = self.id_for(file_name, line_num, choice_num)
		has_tl = len(tl) > 0
		format_str = "Queueing {} of command {} with id:{} and values {} and {} (has_tl={},finished={})"
		print(format_str.format("add" if new else "update", command, id, original, tl, has_tl, finished))
		item = {
			"id": id,
			"lineNum": line_num,
			"file": file_name,
			"original": original,
			"tl": tl,
			"has_tl": has_tl,
			"is_finished": finished,
			"new": new
		}
		self.queue.put(item)

	def search(self, q, page, tl, finished):
		results_obj = {}
		with self.index.searcher() as searcher:
			filter = None
			if tl and finished:
				filter = And(Term("has_tl", "t"), Term("is_finished", "t"))
			elif tl:
				filter = Term("has_tl", "t")
			elif finished:
				filter = Term("is_finished", "t")
			results = searcher.search_page(self.parser.parse(q), page, pagelen=15, filter=filter)
			total_results = len(results)
			results_obj["total_results"] = total_results
			print("Total results: {}".format(total_results))
			for i, result in enumerate(results):
				print("{} - {} ({}, {})".format(i, result["original"], result["file"], result["id"]))
			results_obj["results"] = [dict(hit) for hit in results]
		return results_obj

	def pending_actions(self):
		return self.queue.qsize()

	def execute_queued_item(self, writer, item):
		print("Processing {}".format(item["id"]))
		new = item.pop('new')
		if new:
			writer.add_document(**item)
		else:
			writer.update_document(**item)
		self.queue.task_done()

	def run(self):
		while True:
			print("Queue has {} items".format(self.queue.qsize()))
			with self.index.writer(limitmb=2048) as writer:
				self.execute_queued_item(writer, self.queue.get())
				for i in range(self.batch_size):
					try:
						self.execute_queued_item(writer, self.queue.get_nowait())
					except Empty:
						print("Queue was empty")
						break
			print("Committing state")