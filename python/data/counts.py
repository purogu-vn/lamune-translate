from datetime import date

from python.data.data import Data
from python.data.io import all_files

TL_COUNT_TYPES = ["tl_lines", "tl_characters", "english_characters"]
TL_COUNT_TYPES += [ "finished_{}".format(tl_type) for tl_type in TL_COUNT_TYPES ]
ALL_COUNT_TYPES = ["original_lines", "original_characters"] + TL_COUNT_TYPES

COMMENT_TYPES = ["resolved", "open"]
class CountData(Data):
	def __init__(self, master, directory):
		super().__init__(master, directory.joinpath("counts.json"))

	def fresh_comment_structure(self):
		return { count: 0 for count in COMMENT_TYPES }

	def fresh_count_structure(self):
		return { count: 0 for count in ALL_COUNT_TYPES }

	def fresh_tl_count_structure(self):
		return { count: 0 for count in TL_COUNT_TYPES }

	def count_add_to(self, base, adding):
		for count_type in base:
			base[count_type] += adding[count_type]

	def count_diff(self, count1, count2):
		diff = {}
		for count_type in count1:
			diff[count_type] = count1[count_type] - count2[count_type]
		return diff

	def initial_data(self):
		return {
			"all": self.fresh_count_structure(),
			"by_file": {},
			"by_tag": {},
			"by_day_then_tag": {},
			"comments_by_file": {}
		}

	def today(self):
		return str(date.today())

	def add_count_to_struct(self, count_struct, original, tl, finished):
		count_struct["original_lines"] += 1
		count_struct["original_characters"] += len(original)
		self.add_tl_count_to_struct(count_struct, original, tl, finished)

	def add_tl_count_to_struct(self, count_struct, original, tl, finished):
		self.conditional_add_tl_count(count_struct, original, tl, "", tl or finished)
		self.conditional_add_tl_count(count_struct, original, tl, "finished_", finished)

	def conditional_add_tl_count(self, count_struct, original, tl, prefix, condition):
		if condition:
			count_struct[prefix + "tl_lines"] += 1
			count_struct[prefix + "tl_characters"] += len(original)
			count_struct[prefix + "english_characters"] += len(tl)

	def reset_group(self, group, id):
		self.data[group][id] = self.fresh_count_structure()

	def reset_comments(self, file_name):
		self.data["comments_by_file"][file_name] = self.fresh_comment_structure()

	def get_group_count(self, group, id):
		return self.data[group][id]

	def add_count_to_group(self, group, id, original, tl, finished):
		self.add_count_to_struct(self.get_group_count(group, id), original, tl, finished)

	def add_counts(self, file_name, original, tl, line):
		self.add_count_to_struct(self.data["all"], original, tl, line["finished"])
		self.add_count_to_group("by_file", file_name, original, tl, line["finished"])
		tag = self.master.tag_data.tag_for_file(file_name)
		self.add_count_to_group("by_tag", tag, original, tl, line["finished"])
		total_comments = len(line["comments"]) if "comments" in line else 0
		resolved_comments = sum(1 for comment in line["comments"] if comment["resolved"]) if total_comments > 0 else 0
		self.data["comments_by_file"][file_name]["resolved"] += resolved_comments
		self.data["comments_by_file"][file_name]["open"] += total_comments - resolved_comments


	def count_command(self, command, file_name, line):
		if command == "choice":
			for coriginal, ctl in zip(line["original"], line["tl"]):
				self.add_counts(file_name, coriginal, ctl, line)
		else:
			self.add_counts(file_name, line["original"], line["tl"], line)

	def add_change_counts(self, file_name, original, old_tl, new_tl, old_finished, new_finished):
		today = self.today()
		tag = self.master.tag_data.tag_for_file(file_name)
		if today not in self.data["by_day_then_tag"]:
			self.data["by_day_then_tag"][today] = {}
		if tag not in self.data["by_day_then_tag"][today]:
			self.data["by_day_then_tag"][today][tag] = self.fresh_tl_count_structure()
		old_tl_count = self.fresh_tl_count_structure()
		new_tl_count = self.fresh_tl_count_structure()
		self.add_tl_count_to_struct(old_tl_count, original, old_tl, old_finished)
		self.add_tl_count_to_struct(new_tl_count, original, new_tl, new_finished)
		self.count_add_to(self.data["by_day_then_tag"][today][tag], self.count_diff(new_tl_count, old_tl_count))

	def subtract_file_count_from_all_counts(self, file_name):
		file_counts = self.get_group_count("by_file", file_name)
		tag = self.master.tag_data.tag_for_file(file_name)
		for count in file_counts.keys():
			self.data["all"][count] -= file_counts[count]
			self.data["by_tag"][tag][count] -= file_counts[count]

	def counts_by_tag(self):
		return self.data["by_tag"]

	def counts_by_day_then_tag(self):
		return self.data["by_day_then_tag"]

	def counts_by_file(self):
		return self.data["by_file"]

	def summary_for_file(self, file):
		counts = self.get_group_count("by_file", file) if file != "all" else self.data["all"]
		comment_counts = self.data["comments_by_file"][file] if file != "all" else self.fresh_comment_structure()
		return {
			"counts": counts,
			"comment_counts": comment_counts
		}

	def counts_by_file_summary(self):
		summary = {file: self.summary_for_file(file) for file in all_files() + ["all"]}
		return summary