from python.data.data import Data

MAX_HISTORY = 10
class HistoryData(Data):
	def __init__(self, master, directory):
		super().__init__(master, directory.joinpath("history.json"))

	def initial_data(self):
		return []

	def push_history(self, script):
		if script in self.data:
			self.data.remove(script)
		self.data.append(script)
		if len(self.data) > MAX_HISTORY:
			self.data = self.data[(len(self.data) - MAX_HISTORY):]
		self.commit()

	def get_history(self):
		return self.data