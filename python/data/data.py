from python.data.io import read_json, write_json


class Data:
	def __init__(self, master, path):
		self.master = master
		self.path = path
		self.existing = self.path.exists()
		if not self.existing:
			print("Data for {} not existing, initializing...".format(self.path.name))
		self.data = read_json(self.path) if self.existing else self.initial_data()

	def initial_data(self):
		return {}

	def commit(self):
		write_json(self.path, self.data)
