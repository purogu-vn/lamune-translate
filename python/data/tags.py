from python.data.data import Data

GUESS_MAP = [
	("_names", "names"),
	("03omake", "other"),
	("suzu", "suzuka"),
	("hika", "hikari"),
	("tae", "tae"),
	("nana", "nanami"),
	("om", "omake"),
	("stff", "omake"),
	("mama", "omake"),
	("sindou", "omake"),
	("sakura", "omake"),
	("garasu", "omake"),
	("nendo", "omake"),
	("nero", "nanami"),
	("tero", "tae"),
	("sero", "suzuka"),
	("hero", "hikari")
]

class TagData(Data):
	TAGS = ["nanami", "hikari", "suzuka", "tae", "omake", "other", "names"]

	def __init__(self, master, directory):
		super().__init__(master, directory.joinpath("tags.json"))

	def guess_initial_tag(self, file_name):
		for part, guess in GUESS_MAP:
			if part in file_name:
				tag = guess
				break
		else:
			tag = "other"
		self.data[file_name] = tag

	def tag_for_file(self, file_name):
		return self.data[file_name]

	def all_tags_for_files(self):
		return self.data
