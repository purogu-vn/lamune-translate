import json
from pathlib import Path

from python import config

SCRIPTS_DIR = Path(config.ScriptFolder)
STATIC_DIR = Path(config.WebFolder)
NAMES_FILE = "_names.asb.json"

def read_json(path):
	with path.open('r', encoding="utf8") as f:
		return json.load(f)

def write_json(path, j):
	with path.open("w", encoding="utf8") as f:
		f.write(json.dumps(j, indent=2, ensure_ascii=False))

def read_static(file_name):
	path = STATIC_DIR.joinpath(file_name)
	if path.exists():
		with path.open('rb') as f:
			return f.read()
	else:
		return None

def all_files():
	return [file.name for file in SCRIPTS_DIR.iterdir() if file.name.endswith(".json")]

def process_script_data(file_name, script_data, handle_tl_command, handle_other_command=lambda *args: None):
	for lineNum, line in enumerate(script_data["lines"]):
		if "original" in line and "tl" in line:
			handle_tl_command(line["command"], file_name, line, lineNum)
		else:
			handle_other_command(line["command"], file_name, line, lineNum)

def process_script(file_name, handle_tl_command, handle_other_command=lambda *args: None):
	process_script_data(file_name, read_script(file_name), handle_tl_command, handle_other_command)

def all_script_files():
	return [file.name for file in SCRIPTS_DIR.iterdir() if file.name.endswith('.asb.json')]

def list_all_scripts():
	file_list = all_script_files()
	file_list.insert(0, NAMES_FILE)
	return file_list

def read_script(file_name):
	path = SCRIPTS_DIR.joinpath(file_name)
	return read_json(path)

def write_script(file_name, data):
	path = SCRIPTS_DIR.joinpath(file_name)
	write_json(path, data)

def read_names():
	names = {}
	for line in read_script(NAMES_FILE)["lines"]:
		names[line["original"]] = line["tl"]
	return names