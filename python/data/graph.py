import base64
import struct
from collections import defaultdict

from python.data.data import Data
from python.data.io import all_script_files


class GraphData(Data):
	def __init__(self, master, directory):
		super().__init__(master, directory.joinpath("graph.json"))
		self.file_branch_statuses = defaultdict(dict)
		self.last_seen_choices = None
		self.last_seen_choice_line = None

	def initial_data(self):
		return {
			"forward": defaultdict(list),
			"backward": defaultdict(list),
			"choiceReferences": defaultdict(dict),
		}

	def op_for(self, code):
		if code == 1:
			return "="
		elif code == 2:
			return "2"
		elif code == 3:
			return "3"
		elif code == 4:
			return ">="
		return None

	def reverse_op_for(self, code):
		if code == 1:
			return "!="
		elif code == 2:
			return "rev2"
		elif code == 3:
			return "rev3"
		elif code == 4:
			return "<"
		return None

	def new_branch(self, statuses, name, base=None):
		statuses[name] = {
			"start": 0,
			"script": None,
			"label": None,
			"reward": ""
		} if base is None else {** base }
		return statuses[name]

	def decode_data(self, line):
		return base64.b64decode(line["data"].encode("ascii"))

	def decode_data_as_name(self, line):
		return self.decode_data(line).decode("ascii").strip("\x00")

	def branch_active(self, status):
		return status["start"] <= 0 and status["label"] is None

	def label_active_branches(self, statuses, label):
		self.set_active_branches_property(statuses, "label", label)

	def script_active_branches(self, statuses, script):
		self.set_active_branches_property(statuses, "script", script)

	def jump_active_branches(self, statuses, jump):
		self.set_active_branches_property(statuses, "start", jump)

	def reward_active_branches(self, statuses, reward):
		for status in statuses.values():
			if self.branch_active(status):
				status["reward"] += ", " + reward if len(status["reward"]) else reward

	def set_active_branches_property(self, statuses, prop, val):
		for status in statuses.values():
			if self.branch_active(status):
				status[prop] = val

	def plot_command(self, command, file_name, line, lineNum):
		statuses = self.file_branch_statuses[file_name]
		for status in statuses.values():
			status["start"] -= 1
		if not len(statuses):
			self.new_branch(statuses, "main")

		if command == "branch":
			_, var, val, op = line["arguments"]
			self.jump_active_branches(statuses, line['jump'])
			if var == 0:
				choiceStatus = self.new_branch(statuses, self.last_seen_choices[val - 1])
				choiceStatus["choiceLineNum"] = self.last_seen_choice_line
				choiceStatus["choiceIndex"] = val - 1
			else:
				self.new_branch(statuses, "var{} {} {}".format(var, self.op_for(op), val))
				if op != 1:
					self.new_branch(statuses, "var{} {} {}".format(var, self.reverse_op_for(op), val), statuses['main'])
		elif command == "branchflag":
			_, num_flags, _, _ = line["arguments"]
			self.jump_active_branches(statuses, line['jump'])
			flags = struct.unpack("{}i".format(num_flags), self.decode_data(line))
			multi = len(flags) > 1
			if multi or flags[0] != 110: #skip over repeat scene flag
				flag_str = "flag{}{}".format(",".join([str(flag) for flag in flags]), "s" if multi else "")
				self.new_branch(statuses, flag_str + " set")
				self.new_branch(statuses, flag_str + " not set", statuses['main'])
		elif command == "jump":
			self.jump_active_branches(statuses, line['jump'])
		elif command == "choice":
			self.last_seen_choices = [line["tl"][i] or line["original"][i] for i in range(len(line["original"]))]
			self.last_seen_choice_line = lineNum
		elif command == "load_script" and "script_name" in line:
			self.script_active_branches(statuses, line["script_name"] + ".asb.json")
		elif command == "0x06": #clear_script
			self.script_active_branches(statuses, None)
		elif command == "gotolabel":
			label = self.decode_data_as_name(line)
			if "script_name" in line:
				self.script_active_branches(statuses, line["script_name"] + ".asb.json")
				self.finish_active_branches(statuses, file_name)
			self.label_active_branches(statuses, label)
		elif command == "0x10": # label
			label = self.decode_data_as_name(line)
			for status in statuses.values():
				if status["label"] == label:
					status["label"] = None
		elif command == "0x1C": # add
			_, var, val, _ = line["arguments"]
			self.reward_active_branches(statuses, "var{} + {}".format(var, val))
		elif command == "0x18": # setflag
			flag = line["arguments"][1]
			self.reward_active_branches(statuses, "flag{}".format(flag))
		elif command == "exit":
			self.finish_active_branches(statuses, file_name)


	def finish_active_branches(self, statuses, file_name):
		for branch_name, status in statuses.items():
			if branch_name == "main" and len(statuses.items()) > 1:
				continue
			jump_to = status["script"]
			if jump_to is not None and self.branch_active(status):
				edge_obj = {
					"label": branch_name if branch_name != "main" else "",
					"reward": status["reward"]
				}
				self.data["forward"][file_name].append({**edge_obj, "to": jump_to})
				self.data["backward"][jump_to].append({**edge_obj, "from": file_name})
				if "choiceLineNum" in status:
					fileRefs = self.data["choiceReferences"][file_name]
					lineNum, idx = str(status["choiceLineNum"]), str(status["choiceIndex"])
					if lineNum not in fileRefs:
						fileRefs[lineNum] = {}
					fileRefs[lineNum][idx] = {
						"forward_file": file_name,
						"backward_file": jump_to,
						"forward_idx": len(self.data["forward"][file_name]) - 1,
						"backward_idx": len(self.data["backward"][jump_to]) - 1,
					}

	def update_choice_reference(self, file_name, lineNum, choiceNum, choice):
		reference = self.data["choiceReferences"][file_name][str(lineNum)][str(choiceNum)]
		self.data["forward"][reference["forward_file"]][reference["forward_idx"]]["label"] = choice
		self.data["backward"][reference["backward_file"]][reference["backward_idx"]]["label"] = choice

	def get_graph(self):
		return {
			"nodes": all_script_files(),
			"forward": self.data["forward"],
			"backward": self.data["backward"],
		}