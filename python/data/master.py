from pathlib import Path

from python.data.counts import CountData
from python.data.execute import Executor
from python.data.graph import GraphData
from python.data.history import HistoryData
from python.data.index import ScriptIndexer
from python.data.io import all_files, process_script
from python.data.tags import TagData
from python import config

DATA_DIR = Path(config.DataFolder)
INDEX_DIR = DATA_DIR.joinpath("index")
INDEX_BATCH_SIZE = 700

class DataMaster:

	def __init__(self):
		DATA_DIR.mkdir(exist_ok=True, parents=True)
		self.tag_data = TagData(self, DATA_DIR)
		self.count_data = CountData(self, DATA_DIR)
		self.graph_data = GraphData(self, DATA_DIR)
		self.history_data = HistoryData(self, DATA_DIR)
		self.all_data = [
			self.tag_data,
			self.count_data,
			self.graph_data,
			self.history_data
		]
		self.executor = Executor()
		self.indexer = ScriptIndexer(INDEX_DIR, INDEX_BATCH_SIZE)

		self.setup_process_scripts()
		self.indexer.start()

	def setup_process_scripts(self):
		to_process = [data for data in self.all_data if not data.existing]
		if len(to_process) > 0 or not self.indexer.existing:
			if not self.count_data.existing:
				for tag in self.tag_data.TAGS:
					self.count_data.reset_group("by_tag", tag)
			for file in all_files():
				if not self.tag_data.existing:
					self.tag_data.guess_initial_tag(file)
				if not self.count_data.existing:
					self.count_data.reset_group("by_file", file)
					self.count_data.reset_comments(file)
				process_script(file, self.setup_handle_tl_command, self.setup_handle_other_command)
		else:
			print("All existing data")
		for data in self.all_data:
			data.commit()

	def setup_handle_tl_command(self, command, file_name, line, lineNum):
		if not self.count_data.existing:
			self.count_data.count_command(command, file_name, line)
		if not self.indexer.existing:
			self.indexer.queue_add(command, file_name, line["original"], line["tl"], lineNum, line["finished"])
		if not self.graph_data.existing:
			self.graph_data.plot_command(command, file_name, line, lineNum)

	def setup_handle_other_command(self, command, file_name, line, lineNum):
		if not self.graph_data.existing:
			self.graph_data.plot_command(command, file_name, line, lineNum)


LamuneData = DataMaster()

