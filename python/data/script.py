from python.data.io import read_script, process_script_data, write_script
from python.data.master import LamuneData


class ScriptSaverProcessor:
	def __init__(self, file_name, changes):
		self.file_name = file_name
		self.data = read_script(file_name)
		self.changes = changes
		self.changeTuples = []

	def run(self, lastLine):
		if lastLine is not None:
			self.data["lastLine"] = lastLine
		print("Last line updated to {}".format(lastLine))
		print(LamuneData.count_data.data["all"])
		LamuneData.count_data.subtract_file_count_from_all_counts(self.file_name)
		LamuneData.count_data.reset_group("by_file", self.file_name)
		LamuneData.count_data.reset_comments(self.file_name)

		process_script_data(self.file_name, self.data, self.save_handle_command)
		for original, old_tl, new_tl, old_finished, new_finished in self.changeTuples:
			LamuneData.count_data.add_change_counts(self.file_name, original, old_tl, new_tl, old_finished, new_finished)
		write_script(self.file_name, self.data)

		process_script_data(self.file_name, self.data, self.recount_handle_command)
		LamuneData.count_data.commit()

	def save_handle_command(self, command, _file_name, _line, lineNum):
		if str(lineNum) in self.changes:
			line = self.data["lines"][lineNum]
			change_obj = self.changes[str(lineNum)]
			if "tl" in change_obj or "finished" in change_obj:
				self.handle_tl_change(line, lineNum, command, change_obj)
			if "autoQuote" in change_obj:
				line["autoQuote"] = change_obj["autoQuote"]
			if "comments" in change_obj:
				line["comments"] = change_obj["comments"]
			if "finished" in change_obj:
				line["finished"] = change_obj["finished"]


	def handle_tl_change(self, line, lineNum, command, change_obj):
		updated_finished = change_obj["finished"] if "finished" in change_obj else line["finished"]
		if command == "choice":
			# emulate change_obj structure of { 0: choice0, 1: choice1, ... }
			updated_tl = change_obj["tl"] if "tl" in change_obj else {i: choice for i, choice in enumerate(line["tl"])}
			for sChoice, ctl in updated_tl.items():
				choice = int(sChoice)
				coriginal = line["original"][choice]
				self.changeTuples.append((coriginal, line["tl"][choice], ctl, line["finished"], updated_finished))
				line["tl"][choice] = ctl
				LamuneData.graph_data.update_choice_reference(self.file_name, lineNum, choice, ctl or coriginal)
		else:
			updated_tl = change_obj["tl"] if "tl" in change_obj else line["tl"]
			self.changeTuples.append((line["original"], line["tl"], updated_tl, line["finished"], updated_finished))
			line["tl"] = updated_tl
		line["finished"] = updated_finished

	def recount_handle_command(self, command, file_name, line, lineNum):
		LamuneData.count_data.count_command(command, file_name, line)
		if str(lineNum) in self.changes:
			LamuneData.indexer.queue_update(command, file_name, line["original"], line["tl"], lineNum, line["finished"])