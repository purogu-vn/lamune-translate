from os import startfile
from pathlib import Path
from subprocess import Popen
from zipfile import ZipFile, ZIP_DEFLATED

from python import config

WORKER_EXE = Path(config.ExtractorExe)
GAME_FOLDER = Path(config.GameFolder)
GAME_EXE = GAME_FOLDER.joinpath("lamune-english.exe")
TRANSLATION_ARC = GAME_FOLDER.joinpath("data02.arc")
README_FILE = GAME_FOLDER.joinpath("README.txt")
RELEASE_FOLDER = Path(config.ReleaseFolder)


class Executor:
	def __init__(self):
		self.process = None

	def is_busy(self):
		if self.process:
			alive = self.process.poll() is None
			if not alive:
				self.process = None
				print("Build finished")
			return alive
		return True

	def run_game(self):
		startfile(GAME_EXE)

	def schedule_build(self):
		if not self.process:
			print("Starting build")
			self.process = Popen("{} insert".format(WORKER_EXE))

	def generate_release(self, name):
		RELEASE_FOLDER.mkdir(exist_ok=True, parents=True)
		files = [GAME_EXE, TRANSLATION_ARC, README_FILE]
		for file in files:
			if not file.exists():
				return "Missing {}".format(file.name)
		with ZipFile(RELEASE_FOLDER.joinpath("{}.zip".format(name)), "w", ZIP_DEFLATED) as out:
			for file in files:
				out.write(file, file.name)
		return "Successfully released!"
