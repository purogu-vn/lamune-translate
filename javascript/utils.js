export function safeGet(obj, keys) {
    return safeGetHelper(obj, keys, 0);
}

function safeGetHelper(obj, keys, i) {
    if(i < keys.length) {
        if(obj.hasOwnProperty(keys[i])) {
            return safeGetHelper(obj[keys[i]], keys, i + 1);
        }
        return null;
    }
    return obj;
}

export function safePut(obj, keys, item) {
    let current = obj;
    for(let i = 0; i < keys.length - 1; i++) {
        if(!current.hasOwnProperty(keys[i])) {
            current[keys[i]] = {};
        }
        current = current[keys[i]];
    }
    current[keys[keys.length - 1]] = item;
}

export function safeDelete(obj, keys) {
    safeDeleteHelper(obj, keys, 0);
}

function safeDeleteHelper(obj, keys, i) {
    if(i < keys.length) {
        let deleted = obj.hasOwnProperty(keys[i]) && safeDeleteHelper(obj[keys[i]], keys, i + 1);
        if(deleted) {
            console.log("deleting " + keys[i]);
            delete obj[keys[i]];
            return Object.keys(obj).length === 0;
        }
        return false;
    }
    return true;
}

export const NAMES_FILE = "_names.asb.json";

export function scriptDisplayName(fileName) {
    if(!fileName) {
        return "";
    }
    else if(fileName === NAMES_FILE) {
        return "Name Definitions";
    }
    else {
        return fileName.slice(0, fileName.indexOf('.'));
    }
}

export function quantityAgo(number, unit) {
    return `${number} ${unit}${number > 1 ? "s" : ""} ago`;
}

export function timeAgo(millis) {
    let diff = new Date().getTime() - millis;
    diff = Math.floor(diff / 1000);
    if(diff < 1) {
        return "just now";
    }
    else if (diff < 60) {
        return quantityAgo(diff, "second");
    }
    diff = Math.floor(diff / 60);
    if (diff < 60) {
        return quantityAgo(diff, "minute");
    }
    diff = Math.floor(diff / 60);
    if (diff < 24) {
        return quantityAgo(diff, "hour");
    }
    diff = Math.floor(diff / 24);
    if (diff < 30) {
        return quantityAgo(diff, "day");
    }
    diff = Math.floor(diff / 30);
    if(diff < 12) {
        return quantityAgo(diff, "month");
    }
    return "a long, long time ago";
}

const SCROLL_MAP = {};

// chrome hates multi scroll into view, this can get around it
export function multiScrollIntoView(ele, duration=800, easing='ease-in-out') {
    const parent = ele.parentNode;
    const containerId = parent.id;
    if(SCROLL_MAP.hasOwnProperty(containerId)) {
        window.cancelAnimationFrame(SCROLL_MAP[containerId]);
    }
    const originalY = parent.scrollTop;
    const targetY = ele.offsetTop - parent.offsetHeight/2 + ele.offsetHeight/2;
    const easeFunc = EASING_MAP[easing];
    let startTime;
    const scroll = () => {
        SCROLL_MAP[containerId] = window.requestAnimationFrame((currentTime) => {
            if(!startTime) {
                startTime = currentTime;
            }
            const time = Math.min(1, (currentTime - startTime) / duration);
            parent.scrollTop = easeFunc(time) * (targetY - originalY) + originalY;
            if (time < 1) {
                scroll();
            }
            else {
                delete SCROLL_MAP[containerId];
            }
        });
    };
    SCROLL_MAP[containerId] = window.requestAnimationFrame(() => {
        scroll();
    });
}

const EASING_MAP = {
    'linear'(t) {
        return t;
    },
    'ease-in'(t) {
        return t * t;
    },
    'ease-out'(t) {
        return t * (2 - t);
    },
    'ease-in-out'(t) {
        return t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t;
    }
};

export function isFirefox() {
    return navigator.userAgent.includes("Firefox");
}