import Editor from "./editor";
import {Container, Fixture, Template} from "../templates";
import Stats from "./stats";
import Graph from "./graph";
import Tags from "../tags";
import SpinnerDialog from "../components/spinner_dialog";

const SCREEN_MAP = {
    "editor": Editor,
    "stats": Stats,
    "graph": Graph,
};

const FIRST_SHOWN = "editor";
const Screens = {
    mainScreen: null,
    current: FIRST_SHOWN,
    init: function(mainScreen) {
        Screens.mainScreen = mainScreen;
        SpinnerDialog.show("main-dialog", false);
        Tags.init(() => {
            Screens.initScreens();
        });
    },
    initScreens: function() {
        let screensContainer = Container.fetch("screens");
        let loadingScreens = new Set(Object.keys(SCREEN_MAP));
        Object.keys(SCREEN_MAP).forEach((screenName) => {
            let screenManager = SCREEN_MAP[screenName];
            screenManager.screen = Screens.createScreen(screenName, screensContainer);
            screenManager.init(() => Screens.onScreenFinishInit(loadingScreens, screenName));
        });
        document.onkeydown = Screens.handleKeyDown;
        document.onselectionchange = Screens.handleSelectionChange;
    },
    onScreenFinishInit: function(loadingScreens, screenName) {
        loadingScreens.delete(screenName);
        if(!loadingScreens.size) {
            SpinnerDialog.hide("main-dialog");
        }
    },
    createScreen: function(screenName, screensContainer) {
        let screen = Template.create(`${screenName}_screen`);
        if(screenName === FIRST_SHOWN) {
            Screens.mainScreen.select(`${screenName}-button`);
        }
        else {
            screen.hide();
        }
        Screens.mainScreen.on("click", () => Screens.switchToScreen(screenName), `${screenName}-button`);
        screensContainer.addTemplate(screen);
        return screen;
    },
    toggleScreen: function(screenName, on) {
        const screenManager = SCREEN_MAP[screenName];
        const screen = screenManager.screen;
        Screens.mainScreen.toggleSelected(on,`${screenName}-button`);
        screen.toggleVisible(on);
    },
    switchToScreen: function(to, callback=()=>{}) {
        SpinnerDialog.show("main-dialog", true);
        Screens.toggleScreen(Screens.current, false);
        Screens.toggleScreen(to, true);
        SCREEN_MAP[to].onSwitchTo(() => {
            SpinnerDialog.hide("main-dialog");
            Screens.current = to;
            callback();
        });

    },
    handleKeyDown: function(event) {
        if(event.ctrlKey) {
            let bindings = SCREEN_MAP[Screens.current].COMMAND_MAP;
            if(bindings.hasOwnProperty(event.key)) {
                bindings[event.key]();
                return false;
            }
        }
    },
    findSelection: function() {
        const sel = document.getSelection();
        if(sel.rangeCount > 0) {
            const range = sel.getRangeAt(0).cloneRange();
            if(range.startContainer !== range.endContainer) {
                range.setEndAfter(range.startContainer);
            }
            return range.toString();
        }
        return "";
    },
    handleSelectionChange: function() {
        if(Screens.current === "editor") {
            Editor.changeLookupTerm(Screens.findSelection());
        }
    }
};

export default Screens;