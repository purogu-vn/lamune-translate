
import {
    askGetCountsForDayThenTag,
    askGetCountsForFiles,
    askGetCountsForTags
} from "../api";
import Colors from "../colors";
import SpinnerDialog from "../components/spinner_dialog";
import Tags from "../tags";
import ChartManager from "../chart_manager";



const Stats = {
    busy: false,
    finished: false,
    COMMAND_MAP: {},
    init: function(callback) {
        Stats.screen.on("click", () => Stats.switchFinished(false), "tl-button");
        Stats.screen.on("click", () => Stats.switchFinished(true), "finished-button");
        Stats.screen.disable("tl-button");
        Stats.screen.on("click", () => Stats.switchUnit("characters"), "characters-button");
        Stats.screen.on("click", () => Stats.switchUnit("lines"), "lines-button");
        Stats.screen.disable("characters-button");
        ChartManager.init();
        callback();
    },
    switchFinished: function(newFinished) {
        if(Stats.finished !== newFinished) {
            if(Stats.busy) {
                return;
            }
            Stats.busy = true;
            SpinnerDialog.show("stats-dialog", true);
            Stats.screen.disable(newFinished ? "finished-button" : "tl-button");
            Stats.screen.enable(newFinished ? "tl-button" : "finished-button");
            Stats.finished = newFinished;
            Stats.onSwitchTo(() => {
                Stats.busy = false;
                SpinnerDialog.hide("stats-dialog");
            });
        }
    },
    switchUnit: function(newUnit) {
        if(Stats.unit !== newUnit) {
            if(Stats.busy) {
                return;
            }
            Stats.busy = true;
            SpinnerDialog.show("stats-dialog", true);
            Stats.screen.disable(newUnit === "characters" ? "characters-button" : "lines-button");
            Stats.screen.enable(newUnit === "characters" ? "lines-button" : "characters-button");
            ChartManager.updateUnit(newUnit);
            Stats.onSwitchTo(() => {
                Stats.busy = false;
                SpinnerDialog.hide("stats-dialog");
            });
        }
    },
    onSwitchTo: function(callback) {
        askGetCountsForTags((tagCounts) => {
            askGetCountsForFiles(fileCounts => {
                askGetCountsForDayThenTag((dayTagCounts) => {
                    ChartManager.updateAllCharts(tagCounts, fileCounts, dayTagCounts);
                    callback();
                });
            });
        });
    }
};

export default Stats;