import {askGetGraph} from "../api";
import {Container, Fixture, Template} from "../templates";
import Tags from "../tags";
import Screens from "./screens";
import Editor from "./editor";
import GraphManager from "../graph_manager";
import {scriptDisplayName} from "../utils";

const Graph = {
    navigation: null,
    selectedFile: null,
    COMMAND_MAP: {},
    lazyLoaded: false,
    defaultZoom: 0.8,
    forwardEdges: {},
    backwardEdges: {},
    init: function(callback) {
        Graph.navigation = Fixture.create("graph_navigation");
        Graph.navigation.on("click", Graph.viewEditor, "editor_view");
        Graph.COMMAND_MAP = {
            "e" : Graph.viewEditor
        };
        callback();
    },
    onSwitchTo: function(callback) {
        if(!Graph.lazyLoaded) {
            Graph.lazyLoaded = true;
            
            askGetGraph(({ nodes, forward, backward }) => {
                Graph.forwardEdges = forward;
                Graph.backwardEdges = backward;
                
                GraphManager.init("graph-container", nodes, forward, Graph.onSelectNode, Graph.onDeselectNode);
                Graph.refreshNavigation();
                callback();
            });
        }
        else {
            callback();
        }
    },
    onSelectNode: function(id) {
        Graph.refreshNavigation(id);
    },
    onDeselectNode: function() {
        Graph.refreshNavigation();
    },
    fromEditor: function(id) {
        setTimeout(() => {
            GraphManager.selectNodeJump(id);
        }, 300);
    },
    refreshNavigation: function(id=null) {
        const file = id !== null ? id + ".asb.json" : id;
        Graph.selectedFile = file;
        if(file) {
            Graph.fillJumps(Container.fetch("graph_navigation_previous"), Graph.backwardEdges[file], "from");
            Graph.fillJumps(Container.fetch("graph_navigation_next"), Graph.forwardEdges[file], "to");
            Graph.navigation.fillText("selected", id);
            Graph.navigation.show("editor_view");
            Graph.navigation.setStyle("selected", "backgroundColor", Tags.colorForFile(file));
        }
        else {
            Graph.fillJumps(Container.fetch("graph_navigation_previous"));
            Graph.fillJumps(Container.fetch("graph_navigation_next"));
            Graph.navigation.fillText("selected", "Select a node");
            Graph.navigation.hide("editor_view");
            Graph.navigation.setStyle("selected", "backgroundColor", null);
        }
    },
    fillJumps: function(container, edges, fileKey) {
        edges = edges || [];
        container.clear();
        for (const edgeObject of edges) {
            const file = edgeObject[fileKey];
            const id = scriptDisplayName(file);
            const jump = Template.create("graph_navigation_jump");
            jump.fillText("file", id);
            jump.fillText("label", edgeObject["label"] ? "If: " + edgeObject["label"] : "");
            jump.fillText("reward", edgeObject["reward"] ? "Get: " + edgeObject["reward"] : "");
            jump.tag(Tags.tagForFile(file));
            jump.on("click", () => GraphManager.selectNodeJump(id), "base");
            container.addTemplate(jump);
        }
    },
    viewEditor: function() {
        Screens.switchToScreen("editor");
        if(Graph.selectedFile) {
            Editor.changeFile(Graph.selectedFile);
        }
    }
};

export default Graph;