import {Container, Fixture, Template} from "../templates";
import {isFirefox, NAMES_FILE, safeDelete, safeGet, safePut, scriptDisplayName} from "../utils";
import {
    askGetScriptList,
    askGetNames,
    askSaveChanges,
    askGetScriptContent, askSearchStatus, askBuild, askBuildStatus, askRunGame, askGetStartingFile, askGetCountSummary
} from "../api";
import Progress from "../widgets/progress";
import SearchDialog from "../components/search_dialog";
import Tags from "../tags";
import Screens from "./screens";
import Graph from "./graph";
import StatusChecker from "../status_checker";
import ReleaseDialog from "../components/release_dialog";
import ToggleAction from "../widgets/action";
import CommentDialog from "../components/comment_dialog";
import Autocomplete from "../widgets/autocomplete";
import SpinnerDialog from "../components/spinner_dialog";

const WAIT_BEFORE_LINES_SPINNER = isFirefox() ? 70 : 0;
const SEARCH_RESULT_HIGHLIGHT_TIME = 5000;
const BUILD_STATUS_CHECK_RATE = 500;
const INDEX_STATUS_CHECK_RATE = 2000;
const INSERT_CHARACTERS = ["“", "”", "～", "¥", "ー"];

const Editor = {
    lookupWindow: null,
    statusbar: null,
    totalProgress: null,
    currentLookup: "",
    currentFile: null,
    saveLocked: false,
    currentData: null,
    fileSummary: {},
    names: {},
    // changes[file] = { lines: { ... } }
    // changes[file]["lines"][lineNum] = { tl: text, autoQuote: false, finished: false, comments: [] }
    // changes[file]["lines"][lineNum] = { tl: { 0: text1, 1: text2, 2: text3 } }
    changes: {},
    leftToIndex: 0,
    COMMAND_MAP: {},
    navigationForwardStack: [],
    navigationBackStack: [],
    init: function(callback) {
        Editor.initToolbar();
        Editor.initLookup();
        Editor.totalProgress = Fixture.create("editor-total-progress", "progress");
        Editor.statusbar = Fixture.create("editor_statusbar");
        askGetNames((names) => {
            Editor.names = names;
        });
        askGetCountSummary((fileSummary) => {
            Editor.fileSummary = fileSummary;
            askGetScriptList((files) => {
                Autocomplete.init("file-autocomplete", files);
                let container = Container.fetch("files");
                for(let i = 0; i < files.length; i++) {
                    let temp = Template.create("file_entry", files[i]);
                    let progress = Progress.widget(Template.create("progress", files[i]));
                    temp.tag(Tags.tagForFile(files[i]));
                    temp.fillWidget("progress", progress);
                    temp.fillText("name", scriptDisplayName(files[i]));
                    Editor.updateFileEntry(files[i], temp, progress);

                    temp.on("click", () => Editor.changeFile(files[i]), "entry");
                    container.addTemplate(temp);
                }
                askGetStartingFile((startingFile) => {
                    Editor.updateTotalProgressBar();
                    Editor.checkForSearchStatus();
                    Editor.loadFile(startingFile, callback);
                });
            });
        });
    },
    registerActionAndShortcut: function(buttonId, key, action, actionLabel, keyLabel=key) {
        Editor.COMMAND_MAP[key] = action;
        Editor.screen.on("click", action, buttonId);
        Editor.screen.tooltip(`${actionLabel} (Ctrl ${keyLabel})`, buttonId);
    },
    registerLookupAction: function(id, name, url) {
        Editor.lookupWindow.on("click", () => Editor.lookup(url), id);
        Editor.lookupWindow.tooltip(name, id);
    },
    initToolbar: function() {
        Editor.registerActionAndShortcut("back", "ArrowLeft", Editor.navigateBack, "Back", "←");
        Editor.registerActionAndShortcut("forward", "ArrowRight", Editor.navigateForward, "Forward", "→");
        Editor.registerActionAndShortcut( "save", "s", Editor.save, "Save");
        Editor.registerActionAndShortcut("save-all", "d", Editor.saveAll, "Save all");
        Editor.registerActionAndShortcut("search", "l", Editor.search, "Search");
        Editor.registerActionAndShortcut("graph", "g", Editor.viewGraph, "Graph");
        Editor.registerActionAndShortcut("build", "b", Editor.build, "Build");
        Editor.registerActionAndShortcut("run", "r", Editor.runGame, "Run");
        Editor.registerActionAndShortcut("build-run", "p", Editor.buildAndRun, "Build and Run");
        Editor.registerActionAndShortcut("release", "q", Editor.release, "Release");
        Editor.screen.disable("back");
        Editor.screen.disable("forward");
        Editor.screen.disable("save-all");
        Editor.screen.on("keydown", Editor.searchKeyDown, "search-term");
        for(const char of INSERT_CHARACTERS) {
            const insert = Template.create("insert_character");
            insert.fillText("character", char);
            insert.on("mousedown", (event) => Editor.insertCharacter(event, char));
            Editor.screen.appendTemplate("insert-container", insert);
        }
    },
    initLookup: function() {
        Editor.lookupWindow = Fixture.create("editor_lookup");
        Editor.lookupWindow.on("click", () => window.getSelection().empty(), "close");

        Editor.lookupWindow.on("click", () => SearchDialog.show(Editor.currentLookup), "lamune-search");
        Editor.lookupWindow.tooltip("In-App Search", "lamune-search");
        Editor.registerLookupAction("google", "Google", "https://www.google.com/search?q=");
        Editor.registerLookupAction("jisho", "Jisho", "https://jisho.org/search/");
        Editor.registerLookupAction("google-translate", "Google Translate", "https://translate.google.com/#ja/en/");
        Editor.registerLookupAction("bing-translator", "Bing Translator", "https://www.bing.com/translator/?from=ja&to=en&text=");
        Editor.registerLookupAction("google-images", "Google Images", "https://www.google.com/search?tbm=isch&q=");
        Editor.registerLookupAction("jaded-sfx", "Sound Effects", "http://thejadednetwork.com/sfx/search/?submitSearch=Search+SFX&keyword=");
    },
    onSwitchTo: function(callback) {
        callback();
    },
    goToNameDefinition: function(original) {
        const lineNum = Object.keys(Editor.names).indexOf(original);
        Editor.goToSearchResult(NAMES_FILE, lineNum);
    },
    goToSearchResult: function(file, lineNum) {
        Editor.changeFile(file, true, () => {
            let line = Template.find("line_entry", lineNum);
            line.scrollIntoView();
            line.addClass("line-entry--highlighted");
            setTimeout( () => line.removeClass("line-entry--highlighted"), SEARCH_RESULT_HIGHLIGHT_TIME);
        }, false);
    },
    buildFileInfo: function(file) {
        const counts = Editor.fileSummary[file]["counts"];
        let info = `Translated: ${Editor.calculateSummaryPercentage(file, false).toFixed(2)}%`;
        info += ` (${counts["tl_characters"]} / ${counts["original_characters"]} characters)`;
        info += `\nFinalized: ${Editor.calculateSummaryPercentage(file, true).toFixed(2)}%`;
        info += ` (${counts["finished_tl_characters"]} / ${counts["original_characters"]} characters)`;
        return info;
    },
    calculateSummaryPercentage: function(id, finished=false) {
        const counts = Editor.fileSummary[id]["counts"];
        if(counts["original_characters"]) {
            return counts[(finished ? "finished_" : "") + "tl_characters"] / counts["original_characters"] * 100;
        }
        else {
            return 100;
        }
    },
    updateFileEntry: function(file, entry=null, progress=null) {
        entry = entry || Template.find("file_entry", file);
        progress = progress || Progress.widget(Template.find("progress", file));

        entry.hide("badge");
        entry.tooltip(Editor.buildFileInfo(file), "info");
        const commentCounts = Editor.fileSummary[file]["comment_counts"];
        const resolved = commentCounts["resolved"], open = commentCounts["open"];
        const total = resolved + open;
        entry.toggleVisible(total, "comments-container");
        entry.fillText("comments",  total);
        entry.changeImageSrc(open ? "comment_unresolved.png" : "comment.png", "comments-image");

        progress.setProgress(Editor.calculateSummaryPercentage(file));
    },
    updateTotalProgressBar: function() {
        let progress = Progress.widget(Editor.totalProgress);
        progress.setProgress(Editor.calculateSummaryPercentage("all"), true);
    },
    changeFile: function(file, shouldResetNavigation=true, callback=()=>{}, scrollToLast=true) {
        if(Editor.currentFile === file) {
            callback();
            return;
        }
        if(Editor.currentFile) {
            Template.find("file_entry", Editor.currentFile).unselect();
            if(shouldResetNavigation) {
                Editor.navigationForwardStack = [];
                Editor.screen.disable("forward");
                Editor.navigationBackStack.push(Editor.currentFile);
                Editor.screen.enable("back");
            }
        }
        const timeout = setTimeout(() => SpinnerDialog.show("editor-dialog", false), WAIT_BEFORE_LINES_SPINNER);
        Editor.loadFile(file, () => {
            callback();
            clearTimeout(timeout);
            SpinnerDialog.hide("editor-dialog");
        }, scrollToLast);
    },
    loadFile: function(file, callback, scrollToLast=true) {
        askGetScriptContent(file, (data) => {
            Editor.currentFile = file;
            Editor.screen.toggleEnabled(Editor.currentFileIsChanged(), "save");
            Editor.screen.toggleEnabled(Editor.currentFile !== NAMES_FILE, "graph");
            let fileEntry = Template.find("file_entry", Editor.currentFile);
            fileEntry.select();
            Editor.loadScriptEditor(data);
            Editor.updateStatusbar();
            fileEntry.scrollIntoView();
            const lastLine = Editor.activeLastLine();
            if(scrollToLast && lastLine !== null) {
                Template.find("line_entry", lastLine).scrollIntoView();
            }
            callback();
        });
    },
    loadScriptEditor: function(data) {
        Editor.currentData = data;
        let linesContainer = Container.fetch("lines");
        linesContainer.clear();
        let postActions = [];
        for(let i = 0; i < data.lines.length; i++) {
            const entry = this.createLineEntry(postActions, data.lines[i].command, i);
            if(entry) {
                linesContainer.addTemplate(entry);
                if(postActions) {
                    for(const action of postActions) {
                        action();
                    }
                    postActions = [];
                }
            }
        }
        linesContainer.scrollToTop();
    },
    prepareTlInput: function(entry, lineNum, choiceNum=null) {
        entry.fillInput("tl", Editor.activeTranslation(lineNum, choiceNum));
        entry.on("input", (event) => Editor.changeEntryTl(event, lineNum, choiceNum), "tl");
    },
    prepareTlArea: function(postActions, entry, lineNum) {
        postActions.push(() => entry.resizeToContent("tl"));
        Editor.prepareTlInput(entry, lineNum);
    },
    createLineEntry: function(postActions, command, lineNum) {
        let rendered;
        let spokenLine = false;
        if(command === "text") {
            let speaker = Editor.lineProperty(lineNum, "speaker");
            if(!speaker) {
                rendered = Template.create("narration_line", lineNum);
                rendered.fillText("original", Editor.lineProperty(lineNum, "original"));
                Editor.prepareTlArea(postActions, rendered, lineNum);
            }
            else {
                spokenLine = true;
                rendered = Template.create("spoken_line", lineNum);
                rendered.fillText("original-name", speaker);
                rendered.fillText("tl-name", Editor.names[speaker]);
                rendered.fillText("original", Editor.lineProperty(lineNum, "original"));
                Editor.prepareTlArea(postActions, rendered, lineNum);
                rendered.on("click", () => Editor.goToNameDefinition(speaker), "name-edit");
            }
        }
        else if(command === "window_title") {
            rendered = Template.create("title_line", lineNum);
            rendered.fillText("original", Editor.lineProperty(lineNum, "original"));
            Editor.prepareTlInput(rendered, lineNum);
        }
        else if(command === "load_script") {
            rendered = Template.create("script_line", lineNum);
            const name = Editor.lineProperty(lineNum, "script_name");
            rendered.fillText("load", `Jump to ${name}`);
            rendered.on("click", () => Editor.changeFile(name + ".asb.json"), "load");
        }
        else if(command === "choice") {
            rendered = Template.create("choice_line", lineNum);
            const originalChoices = Editor.lineProperty(lineNum, "original");
            for(let j = 0; j < originalChoices.length; j++) {
                let choiceItem = Template.create("choice_line_item");
                choiceItem.fillText("original", originalChoices[j]);
                Editor.prepareTlInput(choiceItem, lineNum, j);
                rendered.appendTemplate("items", choiceItem);
            }
        }
        else if(command === "name-def") {
            rendered = Template.create("name_def_line", lineNum);
            rendered.fillText("original", Editor.lineProperty(lineNum, "original"));
            Editor.prepareTlInput(rendered, lineNum);
        }

        if(rendered) {
            let wrapper = Template.create("line_entry", lineNum);
            wrapper.fillTemplate("content", rendered);
            if(command === "load_script") {
                wrapper.hide("actions");
            }
            else {
                const commentAction = Template.create("action_comment");
                commentAction.on("click", () => CommentDialog.show(lineNum, Editor.taggedComments(lineNum)));
                commentAction.tooltip("Comments");
                wrapper.appendWidget("actions", commentAction);

                const finishedAction = new ToggleAction("action_finished", Editor.activeFinished(lineNum), (on) => Editor.changeEntryFinished(lineNum, on), "Finalize");
                wrapper.appendWidget("actions", finishedAction);

                if(spokenLine) {
                    const quoteAction = new ToggleAction("action_auto_quote", Editor.activeAutoQuote(lineNum), (on) => Editor.changeEntryAutoQuote(lineNum, on), "Add quotes");
                    wrapper.appendWidget("actions", quoteAction);
                    Editor.quoteSpokenLine(lineNum, Editor.activeAutoQuote(lineNum), wrapper);
                }

                Editor.countCommentsForLine(lineNum, Editor.currentComments(lineNum).length, wrapper);
                Editor.finishLine(lineNum, Editor.activeFinished(lineNum), wrapper);
            }
            if(Editor.lineIsChanged(lineNum)) {
                wrapper.addClass("line-entry--edited");
            }
            return wrapper;
        }
    },
    lineProperty: function(lineNum, property) {
        return Editor.masterGet(Editor.pathForProperty(lineNum, property));
    },
    activeQuality: function(path) {
        const change = Editor.changesGet(Editor.currentFile, path);
        return change !== null ? change : Editor.masterGet(path);
    },
    activeTranslation: function(lineNum, choiceNum=null) {
        return Editor.activeQuality(Editor.pathForTl(lineNum, choiceNum));
    },
    activeAutoQuote: function(lineNum) {
        return Editor.activeQuality(Editor.pathForAutoQuote(lineNum));
    },
    activeFinished: function(lineNum) {
        return Editor.activeQuality(Editor.pathForFinished(lineNum));
    },
    activeLastLine: function() {
        return Editor.activeQuality(Editor.pathForLastLine());
    },
    masterGet: function(path) {
        return safeGet(Editor.currentData, path);
    },
    masterPut: function(path, item) {
        safePut(Editor.currentData, path, item);
    },
    changesHas: function(file, path) {
        return Editor.changesGet(file, path) !== null;
    },
    changesGet: function(file, path) {
        const filePath = Editor.changes[file] || null;
        return filePath === null ? null : safeGet(filePath, path);
    },
    changesPut: function(file, path, item) {
        if(!Editor.changes.hasOwnProperty(file)) {
            Editor.changes[file] = {};
        }
        safePut(Editor.changes[file], path, item);
    },
    changesDelete: function(file, path) {
        safeDelete(Editor.changes, [file, ...path]);
    },
    applyChangeToMaster: function(file, path) {
        const change = Editor.changesGet(file, path);
        if(change !== null) {
            Editor.masterPut(path, change);
            Editor.changesDelete(file, path);
        }
    },
    quoteSpokenLine: function(lineNum, autoQuote, line=null) {
        line = line || Template.find("line_entry", lineNum);
        line.toggleClass("line-entry--unquoted", !autoQuote, "tl-wrapper");
    },
    finishLine: function(lineNum, finished, line=null) {
        line = line || Template.find("line_entry", lineNum);
        line.toggleEnabledWithClass("line-entry__input", !finished);
    },
    countCommentsForLine: function(lineNum, count, line=null) {
        line = line || Template.find("line_entry", lineNum);
        const commentImg = Editor.lineHasCommentChanges(lineNum) ? "comment_change.png" : (Editor.lineHasUnresolvedComments(lineNum) ? "comment_unresolved.png" : "comment.png");
        line.changeImageSrc(commentImg, "comment_image");
        line.fillText("comment_count", count);
    },
    pathForProperty: function(lineNum, property) {
        return ["lines", lineNum, property];
    },
    pathForAutoQuote: function(lineNum) {
        return ["lines", lineNum, "autoQuote"];
    },
    pathForFinished: function(lineNum) {
        return ["lines", lineNum, "finished"];
    },
    pathForComments: function(lineNum) {
        return ["lines", lineNum, "comments"];
    },
    pathForTl: function(lineNum, choiceNum=null) {
        let path = ["lines", lineNum, "tl"];
        if(choiceNum !== null) {
            path.push(choiceNum);
        }
        return path;
    },
    pathForLastLine: function() {
        return ["lastLine"];
    },
    pathForAllOfLine: function(lineNum) {
        return ["lines", lineNum];
    },
    pathForLines: function() {
        return ["lines"];
    },
    applyEntry: function(file, lineNum, choiceNum=null) {
        if(file === Editor.currentFile) {
            Editor.applyChangeToMaster(file, Editor.pathForTl(lineNum, choiceNum));
            Editor.applyChangeToMaster(file, Editor.pathForAutoQuote(lineNum));
            Editor.applyChangeToMaster(file, Editor.pathForComments(lineNum));
            Editor.applyChangeToMaster(file, Editor.pathForFinished(lineNum));
            const lineEntry = Template.find("line_entry", lineNum);
            lineEntry.removeClass("line-entry--edited");
            const commentImg = Editor.lineHasUnresolvedComments(lineNum) ? "comment_unresolved.png" : "comment.png";
            lineEntry.changeImageSrc(commentImg, "comment_image");
        }
        else {
            Editor.changesDelete(file, Editor.pathForAllOfLine(lineNum));
        }
    },
    changeFileEntry: function(newValue, path, equalityTest=Editor.changeBasicEquality) {
        Editor.changeEntry(null, newValue, path, equalityTest);
    },
    changeEntry: function(lineNum, newValue, path, equalityTest=Editor.changeBasicEquality) {
        const editedBefore = Editor.currentFileIsChanged();
        const anyEditedBefore = Editor.anyFileIsChanged();
        if(equalityTest(path, newValue)) {
            Editor.changesDelete(Editor.currentFile, path);
            if(!Editor.lineIsChanged(lineNum) && lineNum !== null) {
                Template.find("line_entry", lineNum).removeClass("line-entry--edited");
            }
        }
        else {
            Editor.changesPut(Editor.currentFile, path, newValue);
            if(lineNum !== null) {
                Template.find("line_entry", lineNum).addClass("line-entry--edited");
            }
        }
        const editedAfter = Editor.currentFileIsChanged();
        const anyEditedAfter = Editor.anyFileIsChanged();
        if(editedBefore !== editedAfter) {
            if(editedBefore) {
                Template.find("file_entry", Editor.currentFile).hide("badge");
                Editor.screen.disable("save");
            }
            else if(editedAfter) {
                Template.find("file_entry", Editor.currentFile).show("badge");
                Editor.screen.enable("save");
            }
            Editor.updateStatusbar();
        }
        if(anyEditedBefore !== anyEditedAfter) {
            Editor.screen.toggleEnabled(anyEditedAfter, "save-all");
        }
    },
    changeBasicEquality: function(path, newValue) {
        return Editor.masterGet(path) === newValue;
    },
    changeLastLineEquality: function(path, newValue) {
        return Editor.changesGet(Editor.currentFile, Editor.pathForLines()) === null || Editor.changeBasicEquality(path, newValue);
    },
    changeCommentEquality: function(lineNum, newComments) {
        const masterComments = Editor.masterComments(lineNum);
        if (masterComments.length !== newComments.length) {
            console.log("unequal");
            return false;
        }
        for(const oldComment of masterComments) {
            let hasNewComment = false;
            for(const newComment of newComments) {
                if(oldComment.timestamp === newComment.timestamp) {
                    if(newComment.text !== oldComment.text) {
                        console.log("different text");
                        return false;
                    }
                    if (newComment.resolved !== oldComment.resolved) {
                        console.log("different resolved");
                        return false;
                    }
                    hasNewComment = true;
                    break;
                }
            }
            if(!hasNewComment) {
                console.log("no new comment");
                return false;
            }
        }
        return true;
    },
    currentComments: function(lineNum) {
        return Editor.changesGet(Editor.currentFile, Editor.pathForComments(lineNum))
            || Editor.masterComments(lineNum);
    },
    masterComments: function(lineNum) {
        return Editor.masterGet(Editor.pathForComments(lineNum)) || [];
    },
    taggedComments: function(lineNum) {
        let iMaster = 0, iCurrent = 0;
        const masterComments = Editor.masterComments(lineNum);
        const currentComments = Editor.currentComments(lineNum);
        const taggedComments = [];
        while(iMaster < masterComments.length && iCurrent < currentComments.length) {
            if(masterComments[iMaster].timestamp === currentComments[iCurrent].timestamp) {
                const tag = masterComments[iMaster].resolved === currentComments[iCurrent].resolved ? "same" : "resolve";
                taggedComments.push({...currentComments[iCurrent], tag: tag});
                iMaster++;
                iCurrent++;
            }
            else if (masterComments[iMaster].timestamp > currentComments[iCurrent].timestamp) {
                taggedComments.push({...currentComments[iCurrent], tag: "new"});
                iCurrent++;
            }
            else {
                taggedComments.push({...masterComments[iMaster], tag: "deleted"});
                iMaster++;
            }
        }
        while(iMaster < masterComments.length) {
            taggedComments.push({...masterComments[iMaster], tag: "deleted"});
            iMaster++;
        }
        while(iCurrent < currentComments.length) {
            taggedComments.push({...currentComments[iCurrent], tag: "new"});
            iCurrent++;
        }
        return taggedComments;
    },
    changeEntryTl: function(event, lineNum, choiceNum) {
        Template.fromEvent(event).resizeToContent();
        const text = event.target.value;
        Editor.changeEntry(lineNum, text, Editor.pathForTl(lineNum, choiceNum));
        Editor.changeFileEntry(lineNum, Editor.pathForLastLine(), Editor.changeLastLineEquality);
    },
    changeEntryAutoQuote: function(lineNum, autoQuote) {
        Editor.quoteSpokenLine(lineNum, autoQuote);
        Editor.changeEntry(lineNum, autoQuote, Editor.pathForAutoQuote(lineNum));
    },
    changeEntryFinished: function(lineNum, finished) {
        Editor.finishLine(lineNum, finished);
        Editor.changeEntry(lineNum, finished, Editor.pathForFinished(lineNum));
    },
    changeCommentEntry: function(lineNum, updatedComments) {
        Editor.changeEntry(lineNum, updatedComments, Editor.pathForComments(lineNum), () => Editor.changeCommentEquality(lineNum, updatedComments));
        Editor.countCommentsForLine(lineNum, updatedComments.length);
    },
    addComment: function(lineNum, commentObj) {
        const updatedComments = Editor.currentComments(lineNum).concat(commentObj);
        Editor.changeCommentEntry(lineNum, updatedComments);
    },
    flipCommentResolved: function(lineNum, timestamp) {
        let newResolved = false;
        const updatedComments = Editor.currentComments(lineNum).map((comment) => {
            if(comment.timestamp === timestamp) {
                newResolved = !comment.resolved;
                return { ...comment, resolved: newResolved };
            }
            else {
                return comment;
            }
        });
        Editor.changeCommentEntry(lineNum, updatedComments);
        return newResolved;
    },
    flipCommentDelete: function(lineNum, timestamp) {
        if(Editor.currentComments(lineNum).some((comment) => comment.timestamp === timestamp)) {
            return Editor.deleteComment(lineNum, timestamp);
        }
        else {
            return Editor.undeleteComment(lineNum, timestamp);
        }
    },
    deleteComment: function(lineNum, timestamp) {
        const deletedMasterComment = Editor.masterComments(lineNum).find((comment) => comment.timestamp === timestamp);
        const updatedComments = Editor.currentComments(lineNum).filter((comment) => comment.timestamp !== timestamp);
        Editor.changeCommentEntry(lineNum, updatedComments);
        return deletedMasterComment ? true : null;
    },
    undeleteComment: function(lineNum, timestamp) {
        const deletedComment = Editor.masterComments(lineNum).find((comment) => comment.timestamp === timestamp);
        const updatedComments = Editor.currentComments(lineNum).concat(deletedComment);
        updatedComments.sort((a, b) => a.timestamp < b.timestamp ? -1 : 1);
        Editor.changeCommentEntry(lineNum, updatedComments);
        return false;
    },
    changedFiles: function() {
        return Object.keys(Editor.changes).length;
    },
    anyFileIsChanged: function() {
        return Editor.changedFiles()  > 0;
    },
    currentFileIsChanged: function() {
        return Editor.changes.hasOwnProperty(Editor.currentFile);
    },
    lineIsChanged: function(lineNum) {
        return Editor.changesHas(Editor.currentFile, Editor.pathForAllOfLine(lineNum));
    },
    lineHasCommentChanges: function(lineNum) {
        return Editor.changesHas(Editor.currentFile, Editor.pathForComments(lineNum));
    },
    lineHasUnresolvedComments: function(lineNum) {
        return Editor.masterComments(lineNum).some((comment) => !comment.resolved);
    },
    save: function() {
        if(Editor.saveLocked) {
            return;
        }
        if(Editor.currentFileIsChanged()) {
            Editor.performSave({
                [Editor.currentFile]: Editor.changes[Editor.currentFile]
            });
        }
    },
    saveAll: function() {
        if(Editor.saveLocked) {
            return;
        }
        if(Editor.anyFileIsChanged()) {
            Editor.performSave(Editor.changes);
        }
    },
    performSave: function(changes) {
        Editor.saveLocked = true;
        Editor.screen.disable("save");
        Editor.screen.disable("save-all");
        askSaveChanges(changes, (fileSummary) => {
            Editor.fileSummary = fileSummary;
            Object.keys(changes).forEach((file) => {
                Editor.updateFileEntry(file);
                Editor.applyFile(file);
            });
            Editor.checkForSearchStatus();
            Editor.updateTotalProgressBar();
            if(Editor.anyFileIsChanged()) {
                Editor.screen.enable("save-all");
            }
            Editor.saveLocked = false;
        });
    },
    applyFile: function(file) {
        if(file === Editor.currentFile) {
            Editor.applyChangeToMaster(file, Editor.pathForLastLine());
        }
        else {
            Editor.changesDelete(file, Editor.pathForLastLine());
        }
        const lineChanges = Editor.changesGet(file, Editor.pathForLines()) || {};
        Object.keys(lineChanges).forEach((lineNum) => {
            if(file === NAMES_FILE) {
                // relies on names remaining in order of insertion
                const original = Object.keys(Editor.names)[lineNum];
                Editor.names[original] = lineChanges[lineNum]["tl"];
                console.log(`name assignment: original=${original} to tl=${lineChanges[lineNum]["tl"]}`);
            }
            if(Array.isArray(lineChanges[lineNum])) {
                Object.keys(lineChanges[lineNum]).forEach((choiceNum) => {
                    Editor.applyEntry(file, lineNum, choiceNum);
                });
            }
            else {
                Editor.applyEntry(file, lineNum);
            }
        });
        console.log(Editor.changes);
    },
    search: function() {
        const value = Editor.screen.value("search-term");
        SearchDialog.show(value);
    },
    searchKeyDown: function(event) {
        if(event.key === "Enter") {
            Editor.search();
        }
    },
    updateStatusbar: function() {
        Editor.statusbar.fillText("file", scriptDisplayName(Editor.currentFile));
        const unsavedFiles = Editor.changedFiles();
        const saveText = unsavedFiles > 0 ? `${unsavedFiles} scripts unsaved` : "Saves up to date";
        Editor.statusbar.fillText("unsaved", saveText);
        const leftText = Editor.leftToIndex ? `Indexing ${Editor.leftToIndex} lines` : "Index up to date";
        Editor.statusbar.fillText("left_to_index", leftText);
    },
    searchStatusUpdate: function(left) {
        Editor.leftToIndex = left;
        Editor.updateStatusbar();
    },
    checkForSearchStatus: function() {
        StatusChecker.activate("search", askSearchStatus, Editor.searchStatusUpdate, ()=>{}, INDEX_STATUS_CHECK_RATE);
	},
    viewGraph: function() {
        if(Editor.currentFile !== NAMES_FILE) {
            Screens.switchToScreen("graph", () => {
                Graph.fromEditor(scriptDisplayName(Editor.currentFile));
            });
        }
    },
    buildComplete: function(andRun) {
        Editor.screen.changeImageSrc("hammer.png", "build-img");
        Editor.screen.enable("build");
        Editor.screen.enable("run");
        Editor.screen.enable("build-run");
        if(andRun) {
            Editor.runGame();
        }
    },
    buildAndRun: function() {
        if(!Editor.screen.isDisabled("build-run")) {
            Editor.runBuild(() => Editor.buildComplete(true));
        }
    },
    build: function() {
        if(!Editor.screen.isDisabled("build")) {
            Editor.runBuild(() => Editor.buildComplete(false));
        }
    },
    runBuild: function(onComplete) {
        Editor.screen.changeImageSrc("gears.svg", "build-img");
        Editor.screen.disable("build");
        Editor.screen.disable("run");
        Editor.screen.disable("build-run");
        askBuild(() => {
            StatusChecker.activate("build", askBuildStatus, ()=>{}, onComplete, BUILD_STATUS_CHECK_RATE);
        });
    },
    runGame: function() {
        if(!Editor.screen.isDisabled("run")) {
            askRunGame();
        }
    },
    release: function() {
        ReleaseDialog.show();
    },
    navigateInDirection: function(fromStack, toStack, fromId, toId) {
        if(!fromStack.length) {
            return;
        }
        const newFile = fromStack.pop();
        if(!fromStack.length) {
            Editor.screen.disable(fromId);
        }
        toStack.push(Editor.currentFile);
        Editor.screen.enable(toId);
        Editor.changeFile(newFile, false);
    },
    navigateForward: function() {
        Editor.navigateInDirection(Editor.navigationForwardStack, Editor.navigationBackStack, "forward", "back");
    },
    navigateBack: function() {
        Editor.navigateInDirection(Editor.navigationBackStack, Editor.navigationForwardStack, "back", "forward");
    },
    changeLookupTerm: function(term) {
        Editor.currentLookup = term.replace(/\\n/g, "");
        Editor.lookupWindow.fillText("term", Editor.currentLookup);
        Editor.lookupWindow.toggleVisible(Editor.currentLookup);
    },
    lookup: function(url) {
        window.open(url + Editor.currentLookup);
    },
    insertCharacter: function(event, char) {
        event.preventDefault();
        const active = document.activeElement;
        const tagName = active.tagName.toLowerCase();
        if(tagName === "input" || tagName === "textarea") {
            const start = active.selectionStart;
            const end = active.selectionEnd;
            active.value = active.value.substring(0, start) + char + active.value.substring(end);
            active.selectionStart = start + 1;
            active.selectionEnd = start + 1;
            active.dispatchEvent(new Event("input"));
        }
    }
};

export default Editor;