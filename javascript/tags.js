import {askGetTaggedFiles, askGetTags} from "./api";
import Colors from "./colors";

const Tags = {
    fileTags: null,
    tags: null,
    init: function(callback) {
        askGetTaggedFiles((fileTags) => {
            Tags.fileTags = fileTags;
            askGetTags((tags) => {
                Tags.tags = tags;
                callback();
            });
        });
    },
    colorForFile: function(file) {
        return Colors.colorForTag(Tags.tagForFile(file));
    },
    borderForFile: function(file) {
        return Colors.borderForTag(Tags.tagForFile(file));
    },
    tagForFile: function(file) {
        return Tags.fileTags[file];
    }
};

export default Tags;