
const START_COLOR = [255, 143, 143];
const MID_COLOR = [255, 247, 140];
const END_COLOR = [162, 255, 143];

export default class Progress {


    constructor(template) {
        this.template = template;
    }

    setProgress(val, label) {
        this.template.setStyle("bar", "width", val + "%");
        let rgb = [];
        for(let i = 0; i < 3; i++) {
            if(val <= 50) {
                rgb[i] = this.interpolate(START_COLOR[i], MID_COLOR[i], val * 2);
            }
            else {
                rgb[i] = this.interpolate(MID_COLOR[i], END_COLOR[i], (val - 50) * 2);
            }
        }
        this.template.setStyle("bar", "background-color", `rgb(${rgb.join(",")})`);
        if(label) {
            this.template.fillText("label", val.toFixed(3) + "%");
        }
    }

    interpolate(start, end, val) {
        let percent = val / 100.0;
        return (start * (1.0 - percent)) + (end * percent);
    }

    render() {
        return this.template;
    }

    static widget(template) {
        return new Progress(template);
    }
}