import {Fixture, Template} from "../templates";
import Tags from "../tags";
import {scriptDisplayName} from "../utils";
import Editor from "../screens/editor";
import {askRecentFiles} from "../api";


export default class Autocomplete {
    constructor(id, scripts) {
        this.currentSelection = 0;
        this.scripts = scripts;
        this.results = [];
        this.query = "";
        this.template = Template.create("autocomplete");
        this.template.on("focus", () => this.onFocus(), "input");
        this.template.on("keydown", (event) => this.onKeydown(event), "input");
        this.template.on("input", (event) => this.onInput(event.target.value), "input");
        this.template.on("blur", () => this.onBlur(), "input");
        Fixture.fetch(id).setTemplate(this.template);
    }

    wrapResultHtml(part, highlight) {
        return highlight ? `<span class="autocomplete__result-highlight">${part}</span>` : part;
    }

    changeSelection(to) {
        const nextTemp = Template.safeFind("autocomplete_result", to);
        if(nextTemp) {
            const thisTemp = Template.safeFind("autocomplete_result", this.currentSelection);
            thisTemp.unselect();
            nextTemp.select();
            nextTemp.scrollPinBottom();
            this.currentSelection = to;
        }
    }

    calculateResults(query) {
        const results = [];
        for(const rawScript of this.scripts) {
            const script = scriptDisplayName(rawScript);
            let iQuery = 0;
            let matchSegments = 0;
            let matchFirstStart = -1;
            let entryHtml = "";
            let lastChain = "";
            let lastWasMatch = false;
            for(let i = 0; i < script.length; i++) {
                const char = script[i];
                if(iQuery < query.length && char.toLowerCase() === query[iQuery]) {
                    if(matchFirstStart < 0) {
                        matchFirstStart = i;
                    }
                    if(!lastWasMatch) {
                        entryHtml += this.wrapResultHtml(lastChain, false);
                        lastChain = "";
                        lastWasMatch = true;
                    }
                    iQuery++;
                }
                else if(lastWasMatch) {
                    entryHtml += this.wrapResultHtml(lastChain, true);
                    matchSegments += 1;
                    lastChain = "";
                    lastWasMatch = false;
                }
                lastChain += char;
            }
            if(iQuery >= query.length) {
                entryHtml += this.wrapResultHtml(lastChain, lastWasMatch);
                if(lastWasMatch) {
                    matchSegments += 1;
                }
                results.push({ html: entryHtml, rawScript: rawScript, segments: matchSegments, firstStart: matchFirstStart });
            }
        }
        results.sort((a, b) => a.segments - b.segments || a.firstStart - b.firstStart);
        return results;
    }

    createResult(rawScript, i, content) {
        const temp = Template.create("autocomplete_result", i);
        if(i === 0) {
            temp.select();
        }
        temp.fillHtml("content", content);
        temp.tag(Tags.tagForFile(rawScript));
        temp.on("mousedown", () => this.onSelect(rawScript));
        this.template.appendTemplate("results-list", temp);
    }

    onInput(query) {
        this.currentSelection = 0;
        this.query = query.trim().toLowerCase();
        this.template.deleteChildren("results-list");
        if(this.query.length > 0) {
            this.results = this.calculateResults(this.query);
            this.template.fillText("message", this.results.length + " results");
            for(let i = 0; i < this.results.length; i++) {
                this.createResult(this.results[i].rawScript, i, this.results[i].html);
            }
            this.template.show("results");
        }
        else {
             this.results = [];
             this.template.fillText("message", "");
             askRecentFiles((files) => {
                 console.log(this.query);
                 if(!this.query) {
                     if(files.length > 0) {
                         this.template.fillText("message", "Recent files");
                     }
                     for(let i = 0; i < files.length; i++) {
                         const recentFile = files[files.length - 1 - i];
                         this.results.push({ rawScript: recentFile });
                         this.createResult(recentFile, i, scriptDisplayName(recentFile));
                     }
                 }

             });
        }
    }

    onKeydown(event) {
        switch(event.key) {
            case "ArrowDown":
                this.changeSelection(this.currentSelection + 1);
                break;
            case "ArrowUp":
                this.changeSelection(this.currentSelection - 1);
                break;
            case "Enter":
                this.onSelect(this.results[this.currentSelection].rawScript);
                this.template.blur("input");
                break;
        }
    }

    onSelect(script) {
        Editor.changeFile(script);
    }

    onFocus() {
        this.template.show("results");
        this.onInput("");
    }

    onBlur() {
        this.template.fillText("message", "");
        this.template.fillInput("input", "");
        this.template.deleteChildren("results-list");
        this.template.hide("results");
    }

    render() {
        return this.template.render();
    }

    static init(id, scripts) {
        new Autocomplete(id, scripts);
    }
}