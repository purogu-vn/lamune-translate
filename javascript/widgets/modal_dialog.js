import {Fixture, Template} from "../templates";

export default class ModalDialog {
    constructor(dialogType, id="main-dialog", mustBeOpen=false) {
        this.dialogFixture = Fixture.fetch(id);
        this.template = Template.create(dialogType);
        this.template.on("click", this.onClose.bind(this), "dialog");
        this.template.on("click", this.preventClose.bind(this), "dialog-content");
        this.template.on("keydown", this.onKeyDown.bind(this));
        this.dialogFixture.setTemplate(this.template);
        this.mustBeOpen = mustBeOpen;
    }

    onKeyDown(event) {
        if(event.key === "Escape") {
            this.onClose();
        }
    }

    onClose() {
        if(!this.mustBeOpen) {
            this.dialogFixture.clearTemplate();
        }
    }

    preventClose(event) {
        event.stopPropagation();
    }

    keepOpen() {
        this.mustBeOpen = true;
    }

    allowClose() {
        this.mustBeOpen = false;
    }

}