import ChartJS from "../lib/canvasjs.min";
import Colors from "../colors";

class Chart {
    constructor(id, title, unit, fixedUnit) {
        this.unit = unit;
        this.fixedUnit = fixedUnit;
        const options = Object.assign({}, this.baseOptions(title), this.getOptions());
        this.chart = new ChartJS.Chart("CHART__" + id, options);
    }

    updateUnit(unit) {
        if(!this.fixedUnit) {
            this.unit = unit;
        }
    }

    baseOptions(title) {
        return {
            title: {
                text: title
            },
            animationEnabled: true,
            theme: "dark2",
        };
    }

    getOptions() {
        return {};
    }

    setData(data) {
        this.chart.options.data[0].dataPoints = data;
    }

    render() {
        this.chart.render();
    }
}

export class PieChart extends Chart {

    updateUnit(unit) {
        super.updateUnit(unit);
        this.chart.options.data[0].toolTipContent = this.tooltip();
    }

    tooltip() {
        return `<span style='\"'color: {color};'\"'>{label}:</span> {y} ${this.unit} #percent%`;
    }

    getOptions() {
        return {
            data: [{
                type: "pie",
                toolTipContent: this.tooltip()
            }]
        };
    }
}

export class PercentBarChart extends Chart {
    getOptions() {
        return {
            axisY: {
                title: "Percentage",
                maximum: 1,
                valueFormatString: "#.##%"
            },
            data: [{
                type: "bar",
                yValueFormatString: "#.##%"
            }]
        };
    }
}

export class ComparisonColumnChart extends Chart {
    updateUnit(unit) {
        super.updateUnit(unit);
        for(const comparison of this.chart.options.data) {
            console.log(comparison);
            comparison.toolTipContent = this.tooltip();
        }
    }

    getOptions() {
        return {
            zoomEnabled: true,
            data: []
        };
    }

    setComparisonData(name, data) {
        const comparison = this.chart.options.data.find(comp => comp.name === name);
        comparison.dataPoints = data;
    }

    tooltip() {
        return `<span style='\"'color: {color};'\"'>{name}:</span> {y} ${this.unit}`;
    }

    addComparison(name) {
        this.chart.options.data.push({
            type: 'column',
            name: name,
            toolTipContent: this.tooltip(),
            dataPoints: []
        });
    }
}

function coloredSpan(content, color) {
    return `<span style= "color:${color}">${content}</span>`;
}

function renderSharedTooltip(e, unit, renderTitle) {
    let anyGreaterThan0 = false;
    let tooltip = "<div>";
    tooltip += `${renderTitle(e.entries[0].dataPoint)}<br/>`;
    for(let i = e.entries.length - 1; i >= 0; i--) {
        const stack = e.entries[i];
        const y = stack.dataPoint.y;
        if(y > 0) {
            tooltip += coloredSpan(stack.dataSeries.name + ": ", stack.dataSeries.color);
            tooltip += `${y} ${unit}<br/>`;
            anyGreaterThan0 = true;
        }
    }
    tooltip += "</div>";
    return anyGreaterThan0 ? tooltip : null;
}

class StackedChart extends Chart {
    findStack(name) {
        return this.chart.options.data.find(stack => stack.name === name);
    }

    clearStackedData() {
        for(const stack of this.chart.options.data) {
            stack.dataPoints = [];
        }
    }

    addStackedData(name, data) {
        this.findStack(name).dataPoints.push(data);
    }

    setAllStackedData(dataGetter) {
        for(const stack of this.chart.options.data) {
            stack.dataPoints = dataGetter(stack.name);
        }
    }
}

export class StackedColumnChart extends StackedChart {
    getOptions() {
        return {
            zoomEnabled: true,
            axisX: {
                valueFormatString: "MMM D YYYY"
            },
            axisY:{
                minimum: 1,
            },
            toolTip: {
                shared: true,
                contentFormatter: (e) => renderSharedTooltip(e, this.unit, (dataPoint) => {
                    return dataPoint.x.toLocaleDateString("en-US");
                })
            },
            data: []
        };
    }

    registerStack(name, color) {
        this.chart.options.data.push({
            type: 'stackedColumn',
            xValueType: "dateTime",
            name: name,
            color: color,
            dataPoints: []
        });
    }
}

export class StackedPercentColumnChart extends StackedChart {
    getOptions() {
        return {
            toolTip: {
                shared: true,
                contentFormatter: (e) => renderSharedTooltip(e, this.unit, (dataPoint) => {
                    return coloredSpan(dataPoint.label, Colors.colorForTag(dataPoint.label));
                })
            },
            data: []
        };
    }

    registerStack(name, color) {
        this.chart.options.data.push({
            type: 'stackedColumn100',
            name: name,
            color: color,
            dataPoints: []
        });
    }
}