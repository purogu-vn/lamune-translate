import {Template} from "../templates";


export default class ToggleAction {
    constructor(name, isOn, onToggle, tooltip) {
        this.template = Template.create(name);
        if(isOn) {
            this.template.hide("inactive");
        }
        else {
            this.template.hide("active");
        }
        this.template.on("click", () => this.onClick());
        this.template.tooltip(tooltip);
        this.onToggle = onToggle;
    }

    onClick() {
        if(this.template.isHidden("active")) {
            this.template.show("active");
            this.template.hide("inactive");
            this.onToggle(true);
        }
        else {
            this.template.hide("active");
            this.template.show("inactive");
            this.onToggle(false);
        }
    }

    render() {
        return this.template.render();
    }
}