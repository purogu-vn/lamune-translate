import cytoscape from "cytoscape";
import dagre from 'cytoscape-dagre';

import Tags from './tags';
import {scriptDisplayName} from "./utils";

cytoscape.use(dagre);

const DEFAULT_ZOOM = 0.8;

const LAYOUT = {
    name: "dagre",
    ranker: "longest-path",
    nodeDimensionsIncludeLabels: true,
    spacingFactor: 1.5,
    rankSep: 70,
    nodeSep: 0,
};

const STYLE = [
    {
        selector: 'node',
        style: {
            'background-color': 'data(backgroundColor)',
            'border-color': 'data(borderColor)',
            'border-width': '2px',
            'min-zoomed-font-size': '12px',
            'label': 'data(id)'
        }
    },
    {
        selector: ':selected',
        style: {
            'background-color': 'data(borderColor)'
        }
    },
    {
        selector: 'edge',
        style: {
            'curve-style': 'bezier',
            'target-arrow-shape': 'triangle-cross',
            'arrow-scale': 2,
            'label': 'data(label)',
            'text-rotation': 'autorotate',
            'min-zoomed-font-size': '12px',
            'text-wrap': 'wrap',
        }
    }
];

const GraphManager = {
    cy: null,
    onSelect: null,
    onDeselect: null,
    init: function(id, nodes, edges, onSelect, onDeselect) {
        GraphManager.onSelect = onSelect;
        GraphManager.onDeselect = onDeselect;
        GraphManager.cy = cytoscape(GraphManager.graphOptions(id, nodes, edges));
        GraphManager.cy.on('select', GraphManager.selectNode, 'node');
        GraphManager.cy.on("click", undefined, GraphManager.deselectNode);
        GraphManager.cy.zoom(DEFAULT_ZOOM);
    },
    graphOptions: function(id, nodes, edges){
        return {
            container: document.getElementById(id),
            elements: GraphManager.formatElements(nodes, edges),
            layout: LAYOUT,
            maxZoom: 2,
            minZoom: 0.15,
            motionBlur: true,
            boxSelectionEnabled: false,
            style: STYLE,
        };
    },
    formatElements: function(nodes, edges) {
		let elements = [];
		nodes.forEach(function(node) {
			elements.push({
				group: "nodes",
				data: {
				    id: scriptDisplayName(node),
                    backgroundColor: Tags.colorForFile(node),
                    borderColor: Tags.borderForFile(node)
				}
			});
		});
		Object.keys(edges).forEach(function(f) {
			edges[f].forEach(function(edgeObject) {
				elements.push({
					group: "edges",
					data: {
						source: scriptDisplayName(f),
						target: scriptDisplayName(edgeObject.to),
                        label: GraphManager.edgeLabel(edgeObject),
					},
                    selectable: false
				});
			});
		});
		return elements;
	},
    edgeLabel: function(edgeObject) {
        if(edgeObject.label || edgeObject.reward) {
            return `${edgeObject.label || "."}\n${edgeObject.reward || "."}`;
        }
        else {
            return "";
        }
    },
    selectNode: function(event) {
        const node = event.target;
        GraphManager.cy.animate({
            center: {
                eles: node
            },
            easing: "ease-out"
        });
        GraphManager.onSelect(node.id());
    },
    selectNodeJump: function(id) {
        GraphManager.cy.$(":selected").unselect();
        GraphManager.cy.$id(id).select();
    },
    deselectNode: function(event) {
        if(event.target === GraphManager.cy) {
            GraphManager.onDeselect();
        }
    },
};

export default GraphManager;