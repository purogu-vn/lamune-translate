const StatusChecker = {
    activeLoops: {},
    activate: function(id, ajax, onResponse, onComplete, timeout) {
        if(StatusChecker.activeLoops.hasOwnProperty(id)) {
            return;
        }
        const doCheck = () => StatusChecker.doStatusRequest(id, ajax, onResponse, onComplete);
        doCheck();
        StatusChecker.activeLoops[id] = setInterval(doCheck, timeout);
    },
    doStatusRequest: function(id, ajax, onResponse, onComplete) {
        ajax(function(left) {
            onResponse(left);
            if(left <= 0) {
                clearInterval(StatusChecker.activeLoops[id]);
                delete StatusChecker.activeLoops[id];
                onComplete();
            }
        });
    }
};

export default StatusChecker;