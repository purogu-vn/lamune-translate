import {Template} from "./templates";
import Screens from "./screens/screens";

let main = Template.create("main");
document.body.appendChild(main.render());
Screens.init(main);