import tinycolor from 'tinycolor2';

const Colors = {
    colorMap: {
        nanami: "#f9e6ff",
        hikari: "#ffffb3",
        suzuka: "#ccd9ff",
        tae: "#ffcce0",
        omake: "#e6faff",
        other: "#ffffff",
        names: "#d2ffc7"
    },
    borderMap: {},
    completionMap: {
        "none" : "#a3d5ff",
        "started" : "#ffdb61",
        "tl" : "#e7ffaa",
        "finalized" : "#ccfff7"
    },
    colorForCompletion: function(completion) {
        return Colors.completionMap[completion];
    },
    colorForTag: function(tag) {
        return Colors.colorMap[tag];
    },
    borderForTag: function(tag) {
        if(!Colors.borderMap.hasOwnProperty(tag)) {
            const color = tinycolor(Colors.colorForTag(tag));
            Colors.borderMap[tag] = "#" + color.darken(20).toHex();
        }
        return Colors.borderMap[tag];
    }
};

export default Colors;