import {isFirefox, multiScrollIntoView} from "./utils";

export class Template {

    constructor(element) {
        this.element = element;
    }

    _lookupChild(id) {
        if(!id) {
            return this.element;
        }
        let cls = "DATA__" + id;
        return this.element.classList.contains(cls) ? this.element : this.element.querySelector("." + cls);
    }

    hasChild(id) {
        return this._lookupChild(id);
    }

    findChild(id) {
        let child = this._lookupChild(id);
        if(!child) {
            throw new Error("Child not found in template: " + id);
        }
        return child;
    }

    findAllChildren(cls) {
        return this.element.querySelectorAll("." + cls);
    }

    clone(newId) {
        const cloneElement = this.element.cloneNode(true);
        cloneElement.setAttribute("id", newId);
        return new Template(cloneElement);
    }

    fillHtml(id, html) {
        this.findChild(id).innerHTML = html;
    }

    fillText(id, text) {
        this.findChild(id).innerText = text;
    }

    fillInput(id, text) {
        this.findChild(id).value = text;
    }

    fillTemplate(id, content) {
        let old = this.findChild(id);
        old.parentNode.replaceChild(content.render(), old);
    }

    fillWidget(id, widget) {
        this.fillTemplate(id, widget.render());
    }

    value(id) {
        return this.findChild(id).value;
    }

    setStyle(id, prop, val) {
        this.findChild(id).style[prop] = val;
    }

    resizeToContent(id=null) {
        const child = this.findChild(id);
        child.style.height = "auto";
        child.style.height = child.scrollHeight + 6 + "px";
    }

    focus(id) {
        this.findChild(id).focus();
    }

    blur(id) {
        this.findChild(id).blur();
    }

    changeImageSrc(src, id=null) {
        this.findChild(id).src = src;
    }

    appendTemplate(id, content) {
        this.findChild(id).appendChild(content.render());
    }

    appendWidget(id, content) {
        this.appendTemplate(id, content);
    }

    deleteChildren(id) {
        const child = this.findChild(id);
        child.parentNode.replaceChild(child.cloneNode(false), child);
    }

    delete(id) {
        this.findChild(id).remove();
    }

    select(id=null) {
        this.addClass("selected", id);
    }

    unselect(id=null) {
        this.removeClass("selected", id);
    }

    toggleSelected(select, id=null) {
        if(select) {
            this.select(id);
        }
        else {
            this.unselect(id);
        }
    }

    isHidden(id=null) {
        return this.findChild(id).classList.contains("hidden");
    }

    hide(id=null) {
        this.addClass("hidden", id);
    }

    show(id=null) {
        this.removeClass("hidden", id);
    }

    toggleVisible(visible, id=null) {
        if(visible) {
            this.show(id);
        }
        else {
            this.hide(id);
        }
    }

    addClass(cls, id=null) {
        this.findChild(id).classList.add(cls);
    }

    removeClass(cls, id=null) {
        this.findChild(id).classList.remove(cls);
    }

    toggleClass(cls, on, id=null) {
        if(on) {
            this.addClass(cls, id);
        }
        else {
            this.removeClass(cls, id);
        }
    }

    toggleEnabledWithClass(cls, enable) {
        if(enable) {
            this.enableWithClass(cls);
        }
        else {
            this.disableWithClass(cls);
        }
    }

    enableWithClass(cls) {
        this.findAllChildren(cls).forEach((element) => {
            element.disabled = false;
        });
    }

    disableWithClass(cls) {
        this.findAllChildren(cls).forEach((element) => {
            element.disabled = true;
        });
    }

    tooltip(text, id=null) {
        this.findChild(id).title = text;
    }

    disable(id=null) {
        this.findChild(id).disabled = true;
    }

    enable(id=null) {
        this.findChild(id).disabled = false;
    }

    toggleEnabled(on, id=null) {
        if(on) {
            this.enable(id);
        }
        else {
            this.disable(id);
        }
    }

    isDisabled(id=null) {
        return this.findChild(id).disabled;
    }

    tag(tag, id=null) {
        this.addClass(`tag--${tag}`, id);
    }

    scrollIntoView() {
        isFirefox() ? this.element.scrollIntoView({ behavior: "smooth", block: "center" }) : multiScrollIntoView(this.element);
    }

    scrollPinBottom() {
        this.element.scrollIntoView(false);
    }

    scrollToBottom(id=null) {
        const ele = this.findChild(id);
        ele.scrollTop = ele.scrollHeight;
    }

    on(event, callback, id = null) {
        this.findChild(id).addEventListener(event, callback);
    }

    render() {
        return this.element;
    }

    static instanceId(temp, id) {
        return `template__${temp}__instance__${id}`;
    }

    static create(temp, id=null) {
        let template = document.getElementById(`template__${temp}`);
        if(!template) {
            throw new Error("Can't find template with id " + temp);
        }
        let element = document.importNode(template.content, true).children[0];
        if(id !== null) {
            element.setAttribute("id", this.instanceId(temp, id));
        }
        return new Template(element);
    }

    static clone(temp, id, newId=null) {
        const instance = Template.find(temp, id);
        return instance.clone(newId);
    }

    static safeFind(temp, id) {
        let element = document.getElementById(this.instanceId(temp, id));
        return element ? new Template(element) : null;
    }

    static find(temp, id) {
        let element = document.getElementById(this.instanceId(temp, id));
        if(!element) {
            throw new Error(`Can't find instance of template ${temp} with id ${id}`);
        }
        return new Template(element);
    }

    static fromEvent(event) {
        return new Template(event.target);
    }
}

export class Container {
    constructor(id) {
        this.element = document.getElementById("CONTAINER__" + id);
        if(!this.element) {
            throw new Error("Can't find container with id " + id);
        }
    }

    scrollToTop() {
        this.element.scrollTop = 0;
    }

    addTemplate(t) {
        this.element.appendChild(t.render());
    }

    clear() {
        let range = document.createRange();
        range.selectNodeContents(this.element);
        range.deleteContents();
    }

    static fetch(id) {
        return new Container(id);
    }
}

export class Fixture {
    constructor(id) {
        this.id = Fixture.fixtureId(id);
        this.element = document.getElementById(this.id);
        if(!this.element) {
            throw new Error("Can't find fixture with id " + id);
        }
    }

    setTemplate(temp) {
        let content = temp.render();
        this.element.parentNode.replaceChild(content, this.element);
        this.element = content;
        this.element.setAttribute("id", this.id);
    }

    clearTemplate() {
        this.setTemplate(Template.create("blank_fixture"));
    }

    getStyle(style) {
        return window.getComputedStyle(this.element)[style];
    }

    static fixtureId(id) {
        return `FIXTURE__${id}`;
    }

    static fetch(id) {
        return new Fixture(id);
    }

    static create(id, tempId=id) {
        const temp = Template.create(tempId);
        Fixture.fetch(id).setTemplate(temp);
        return temp;
    }
}