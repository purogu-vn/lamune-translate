import {
    ComparisonColumnChart,
    PercentBarChart,
    PieChart,
    StackedColumnChart,
    StackedPercentColumnChart
} from "./widgets/chart";
import Colors from './colors';
import Tags from "./tags";

const COMPLETION_LABELS = {
    "none" : "Not started",
    "started" : "In Progress",
    "tl" : "Translated",
    "finalized" : "Finalized"
};

const ChartManager = {
    charts: {},
    unit: 'characters',
    prepareCharts: function() {
        ChartManager.registerChart(PieChart, "original-by-tag", "Tag breakdown");
        ChartManager.registerChart(PercentBarChart, "progress-by-tag", "Tag progress");
        ChartManager.registerChart(PieChart, "file-completion", "File progress", "files");
        ChartManager.registerChart(ComparisonColumnChart, "length-comparison-by-tag", "Translation length comparison by tag", "characters");
        ChartManager.registerChart(StackedColumnChart,"progress-by-day-tag", "Day progress by tag");
        ChartManager.registerChart(StackedPercentColumnChart, "file-completion-by-tag", "File progress by tag", "files");
        
    },
    createCharts: function() {
        // create all charts by executing lambdas
        for(const id of Object.keys(ChartManager.charts)) {
            ChartManager.charts[id] = ChartManager.charts[id]();
        }
        ChartManager.charts["length-comparison-by-tag"].addComparison("Japanese");
        ChartManager.charts["length-comparison-by-tag"].addComparison("English");
        for(const tag of Tags.tags) {
            ChartManager.charts["progress-by-day-tag"].registerStack(tag, Colors.colorForTag(tag));
        }
        for(const [completion, label] of Object.entries(COMPLETION_LABELS)) {
            ChartManager.charts["file-completion-by-tag"].registerStack(label, Colors.colorForCompletion(completion));
        }
    },
    init: function() {
        ChartManager.prepareCharts();
        ChartManager.createCharts();
    },
    registerChart: function(type, id, title, unit=null) {
        // prepare chart with lambda, don't actually make it yet
        const fixedUnit = unit !== null;
        ChartManager.charts[id] = () => new type(id, title, unit || ChartManager.unit, fixedUnit);
    },
    keyFor: function(type, unit=ChartManager.unit, finished=ChartManager.finished) {
        if(type === "english") {
            unit = "characters";
        }
        if (type === "original") {
            finished = false;
        }
        return `${finished ? "finished_" : ""}${type}_${unit}`;
    },
    stringToDate: function(str) {
        const parts = str.split("-");
        return new Date(parts[0], parts[1]-1, parts[2]);
    },
    makeTagData: function(tagCounts, yMaker) {
        return Object.keys(tagCounts).map((tag) => {
            return {
                y: yMaker(tagCounts[tag]),
                indexLabelFontColor: Colors.colorForTag(tag),
                color: Colors.colorForTag(tag),
                label: tag
            };
        });
    },
    makeTagDataNoColor: function(tagCounts) {
        return Object.keys(tagCounts).map((tag) => {
            return {
                y: tagCounts[tag],
                label: tag
            };
        });
    },
    makeFileCompletionData: function(completionCounts) {
        return Object.keys(completionCounts).map((completion) => {
            return {
                y: completionCounts[completion],
                indexLabelFontColor: Colors.colorForCompletion(completion),
                color: Colors.colorForCompletion(completion),
                label: COMPLETION_LABELS[completion]
            };
        });
    },
    fileCompletionLabel: function(fileCount) {
        const originalLines = fileCount[ChartManager.keyFor("original")];
        const finishedLines = fileCount[ChartManager.keyFor("tl", ChartManager.unit, true)];
        const tlLines = fileCount[ChartManager.keyFor("tl", ChartManager.unit, false)];
        if(originalLines === finishedLines) {
            return "finalized";
        }
        else if(originalLines === tlLines) {
            return "tl";
        }
        else if(tlLines > 0) {
            return "started";
        }
        else {
            return "none";
        }
    },
    updateUnit: function(newUnit) {
        ChartManager.unit = newUnit;
        for(const chart of Object.values(ChartManager.charts)) {
            chart.updateUnit(ChartManager.unit);
        }
    },
    updateAllCharts: function(tagCounts, fileCounts, dayTagCounts) {
        ChartManager.charts["original-by-tag"].setData(ChartManager.makeTagData(tagCounts, (counts) => counts[ChartManager.keyFor("original")]));
        ChartManager.charts["progress-by-tag"].setData(ChartManager.makeTagData(tagCounts, (counts) => counts[ChartManager.keyFor("tl")] / counts[ChartManager.keyFor("original")]));
        ChartManager.charts["length-comparison-by-tag"].setComparisonData("Japanese", ChartManager.makeTagData(tagCounts, (counts) => counts[ChartManager.keyFor("tl", "characters")]));
        ChartManager.charts["length-comparison-by-tag"].setComparisonData("English", ChartManager.makeTagData(tagCounts, (counts) => counts[ChartManager.keyFor("english", "characters")]));
        let completionCounts = {};
        let completionTagCounts = {};
        for(const completion of Object.keys(COMPLETION_LABELS)) {
            completionCounts[completion] = 0;
            completionTagCounts[completion] = {};
            for(const tag of Tags.tags) {
                completionTagCounts[completion][tag] = 0;
            }
        }
        for (const [file, count] of Object.entries(fileCounts)) {
            const label = ChartManager.fileCompletionLabel(count);
            completionCounts[label]++;
            completionTagCounts[label][Tags.tagForFile(file)]++;
        }
        ChartManager.charts["file-completion"].setData(ChartManager.makeFileCompletionData(completionCounts));
        ChartManager.charts["file-completion-by-tag"].setAllStackedData((completionLabel) => {
            const completion = Object.keys(COMPLETION_LABELS).find((comp) => COMPLETION_LABELS[comp] === completionLabel);
            return ChartManager.makeTagDataNoColor(completionTagCounts[completion]);
        });
        ChartManager.charts["progress-by-day-tag"].clearStackedData();
        Object.keys(dayTagCounts).forEach((day) => {
            Object.keys(dayTagCounts[day]).forEach((tag) => {
                const counts = dayTagCounts[day][tag];
                ChartManager.charts["progress-by-day-tag"].addStackedData(tag, {
                    x: ChartManager.stringToDate(day),
                    y: counts[ChartManager.keyFor("tl")]
                });
            });
        });
        Object.values(ChartManager.charts).forEach((chart) => {
            chart.render();
        });
    }
};

export default ChartManager;