function json_fetch(url, callback, options={}) {
	fetch(url, options)
		.then(response => {

			if(response.ok) {
				const contentType = response.headers.get("content-type");
				if(contentType && contentType.includes("application/json")) {
					return response.json();
				}
				else {
					return response;
				}
			}
			else {
				console.error(response);
				throw new Error(response.status);
			}
		})
		.then(response => {
			try {
				callback(response);
			}
			catch(e) {
				console.error(e);
				throw e;
			}
		})
		.catch(error => {
		if (error.name === 'AbortError') {
			alert('abort');
		}
		else {
			alert(error);
		}
	});
}

function json_post(url, data, callback) {
	json_fetch(url, callback, {
		method: "POST",
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(data)
	});
}

// editor
export function askGetScriptList(callback) {
	return json_fetch('/scripts/list', callback);
}

export function askGetScriptContent(file_name, callback) {
	return json_fetch('/scripts/content?file_name=' + file_name, callback);
}

export function askGetNames(callback) {
	json_fetch('/scripts/names', callback);
}

export function askSaveChanges(changes, callback) {
	json_post('/scripts/save/', changes, callback);
}

export function askRecentFiles(callback) {
	return json_fetch('/scripts/history/', callback);
}

export function askGetStartingFile(callback) {
	return json_fetch('/scripts/recent/', callback);
}

export function askGetTaggedFiles(callback) {
	return json_fetch('/tags/for_files', callback);
}

export function askGetCountSummary(callback) {
	return json_fetch('/counts/for_files_summary', callback);
}

export function askGetCountFileComments(callback) {
	return json_fetch('/counts/comments_for_files', callback);
}

export function askSearchFiles(term, page, tl, finished, callback) {
	return json_fetch(`/search?term=${term}&page=${page}&tl=${tl ? "t" : "f"}&finished=${finished ? "t" : "f"}`, callback);
}

export function askSearchStatus(callback) {
	return json_fetch('/search/status', callback);
}

export function askBuild(callback) {
	return json_fetch('/execute/build', callback);
}

export function askBuildStatus(callback) {
	return json_fetch('/execute/build_status', callback);
}

export function askRunGame() {
	return json_fetch('/execute/run', ()=>{});
}

export function askRelease(name, callback) {
	return json_fetch('/execute/release?name=' + name, callback);
}


// stats
export function askGetCountsForTags(callback) {
	return json_fetch('/counts/for_tags', callback);
}

export function askGetCountsForFiles(callback) {
	return json_fetch('/counts/for_files', callback);
}

export function askGetCountsForDayThenTag(callback) {
	return json_fetch('/counts/for_day_then_tag', callback);
}

export function askGetTags(callback) {
	return json_fetch('/tags', callback);
}

// graph
export function askGetGraph(callback) {
	return json_fetch('/graph', callback);
}