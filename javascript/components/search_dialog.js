import {Container, Fixture, Template} from "../templates";
import {askSearchFiles} from "../api";
import {scriptDisplayName} from "../utils";
import Editor from "../screens/editor";
import Tags from "../tags";
import ModalDialog from "../widgets/modal_dialog";


const RESULTS_PER_PAGE = 15;

export default class SearchDialog extends ModalDialog {
    constructor(term) {
        super("search_dialog");
        this.template.on("input", (event) => this.onTermChange(event.target.value), "term");
        this.spinner = Template.create("spinner");
        this.spinner.hide();
        this.template.fillTemplate("spinner", this.spinner);
        this.template.focus("term");
        this.resultsContainer = Container.fetch("search_results");
        this.activeDebounce = null;
        this.activeRequest = null;
        this.lastSearched = null;
        this.lastTotalResults = null;
        this.page = 1;
        this.tl = false;
        this.finished = false;
        this.template.fillInput("term", term);
        this.template.on("click", () => this.onPageForward(), "next");
        this.template.on("click", () => this.onPageBack(), "previous");
        this.template.on("change", (event) => this.onTlCheck(event.target.checked), "tl");
        this.template.on("change", (event) => this.onFinishedCheck(event.target.checked), "finished");
        this.template.hide("results-toolbar");
        this.updatePage();
        this.onTermChange(term);
    }

    onTlCheck(checked) {
        this.tl = checked;
        this.onTermChange(this.lastSearched);
    }

    onFinishedCheck(checked) {
        this.finished = checked;
        this.onTermChange(this.lastSearched);
    }

    updatePage() {
        this.template.fillText("page", this.page);
        this.template.disable("next");
        this.template.disable("previous");
    }

    onPageForward() {
        this.page += 1;
        this.updatePage();
        this.onTermChange(this.lastSearched);
    }

    onPageBack() {
        this.page -= 1;
        this.updatePage();
        this.onTermChange(this.lastSearched);
    }

    handleNavigation() {
        this.template.fillText("page", this.page);
        const totalPages = Math.ceil(this.lastTotalResults / RESULTS_PER_PAGE);
        if(this.page + 1 <= totalPages) {
            this.template.enable("next");
        }
        if(this.page > 1) {
            this.template.enable("previous");
        }
    }

    onResultClick(result) {
        console.log(result);
        this.template.blur("term");
        Editor.goToSearchResult(result.file, result.lineNum);
        this.onClose();
    }

    debounceSearchRequest(term) {
        let me = this;
        me.activeDebounce = setTimeout(() => {
            if(me.activeRequest) {
                me.activeRequest.abort();
            }
            this.lastSearched = term;
            me.resultsContainer.clear();
            this.spinner.show();
            me.activeRequest = askSearchFiles(term, this.page, this.tl, this.finished, (resultSummary) => {
                const results = resultSummary["results"];
                this.spinner.hide();
                this.lastTotalResults = resultSummary["total_results"];
                if(results.length === 0) {
                    let message = Template.create("search_dialog_message");
                    message.fillText("text", "No results");
                    me.resultsContainer.addTemplate(message);
                    me.page = 1;
                    me.template.hide("results-toolbar");
                }
                else {
                    me.template.fillText("total", `Found ${this.lastTotalResults} results`);
                    me.template.show("results-toolbar");
                }
                results.forEach((result) => {
                    let line = Template.create("search_dialog_result");
                    line.fillText("file", scriptDisplayName(result.file));
                    line.fillText("original", result.original);
                    line.fillText("tl", result.tl);
                    line.tag(Tags.tagForFile(result.file), "result");
                    line.on("click", () => me.onResultClick(result), "result");
                    if(!isNaN(result.lineNum)) {
                        line.fillText("line-num", " (line " + result.lineNum + ")");
                    }
                    me.resultsContainer.addTemplate(line);
                });
                me.handleNavigation();
            });
        }, 500);
    };

    onTermChange(term) {
        if(this.activeDebounce) {
            clearTimeout(this.activeDebounce);
        }
        if(term) {
            this.debounceSearchRequest(term);
        }
        else {
            this.resultsContainer.clear();
            this.template.hide("results-toolbar");
            this.template.fillText("total", "");
            this.page = 1;
            this.lastSearched = null;
            this.lastTotalResults = null;
        }
    };

    static show(term) {
        new SearchDialog(term);
    }
}