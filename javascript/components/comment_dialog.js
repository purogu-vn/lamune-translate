import ModalDialog from "../widgets/modal_dialog";
import Editor from "../screens/editor";
import {Template} from "../templates";
import {timeAgo} from "../utils";

export default class CommentDialog extends ModalDialog {
    constructor(lineNum, taggedComments) {
        super("comment_dialog");
        this.lineNum = lineNum;
        const reference = Template.clone("line_entry", lineNum);
        if(reference.hasChild("name-edit")) {
            reference.hide("name-edit");
        }
        reference.removeClass("line-entry--edited");
        reference.addClass("line-entry--reference");
        reference.disableWithClass("line-entry__input");
        reference.hide("actions");
        this.template.fillTemplate("reference", reference);
        reference.resizeToContent("tl");
        for(const comment of taggedComments) {
            this.template.appendTemplate("comments", this.createCommentEntry(comment, comment.tag === "new"));
        }
        this.template.scrollToBottom("comments");
        this.template.on("input", (event) => this.onCommentChange(event.target.value), "comment_input");
        this.template.disable("comment_submit");
        this.template.on("click", this.onSubmit.bind(this), "comment_submit");
        this.comment = "";
    }

    createCommentEntry(comment, newlyCreated) {
        const commentEntry = Template.create("comment_entry", comment.timestamp);
        this.fillResolve(commentEntry, comment.resolved);
        this.fillDelete(commentEntry, comment.tag === "deleted");
        commentEntry.fillText("date", timeAgo(comment.timestamp));
        commentEntry.tooltip(new Date(comment.timestamp), "date");
        commentEntry.fillText("comment", comment.text);
        commentEntry.on("click", () => this.onResolve(comment.timestamp), "resolve");
        commentEntry.on("click", () => this.onDelete(comment.timestamp), "delete");
        if(newlyCreated) {
            commentEntry.addClass("comment-entry--new");
        }
        return commentEntry;
    }

    fillDelete(commentEntry, deleted) {
        commentEntry.toggleClass("comment-entry--deleted", deleted);
        commentEntry.changeImageSrc(deleted ? "undo.png" : "cross.png", "delete-image");
        commentEntry.toggleEnabled(!deleted, "resolve");
        commentEntry.fillText("delete-text", deleted ? "Undo" : "Delete");
    }

    fillResolve(commentEntry, resolved) {
        commentEntry.fillText("resolve-text", resolved ? "Resolved" : "Open");
        commentEntry.changeImageSrc(resolved ? "check-mark.png" : "exclamation-mark.png", "resolve-image");
        commentEntry.toggleClass("comment-entry--resolved", resolved);
    }

    onResolve(timestamp) {
        const resolved = Editor.flipCommentResolved(this.lineNum, timestamp);
        this.fillResolve(Template.find("comment_entry", timestamp), resolved);
    }

    onDelete(timestamp) {
        const deleted = Editor.flipCommentDelete(this.lineNum, timestamp);
        const commentEntry = Template.find("comment_entry", timestamp);
        if(deleted === null) {
            commentEntry.delete();
        }
        else {
            this.fillDelete(commentEntry, deleted);
        }
    }

    onCommentChange(comment) {
        this.template.toggleEnabled(comment, "comment_submit");
        this.comment = comment;
    }

    onSubmit() {
        if(!this.comment) {
            return;
        }
        const createdComment = {
            timestamp: new Date().getTime(),
            text: this.comment,
            resolved: false
        };
        this.template.fillInput("comment_input", "");
        this.onCommentChange("");
        this.template.appendTemplate("comments", this.createCommentEntry(createdComment, true));
        Editor.addComment(this.lineNum, createdComment);
        this.template.scrollToBottom("comments");
    }

    static show(lineNum, comments) {
        new CommentDialog(lineNum, comments);
    }
}