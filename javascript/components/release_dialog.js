import ModalDialog from "../widgets/modal_dialog";
import {askRelease} from "../api";
import {Template} from "../templates";


export default class ReleaseDialog extends ModalDialog {
    constructor() {
        super("release_dialog");
        this.template.on("click", this.onRelease.bind(this), "release");
        this.template.on("input", this.onNameChange.bind(this), "name");
        this.template.fillTemplate("spinner", Template.create("spinner"));
        this.template.disable("release");
        this.template.hide("finish_screen");
        this.template.hide("waiting_screen");
        this.name = "";
    }

    onNameChange(event) {
        let name = event.target.value;
        this.template.toggleEnabled(name, "release");
        this.name = name;
    }

    onRelease() {
        this.keepOpen();
        this.template.show("waiting_screen");
        this.template.hide("enter_screen");
        askRelease(this.name, (message) => {
            this.allowClose();
            this.template.show("finish_screen");
            this.template.hide("waiting_screen");
            this.template.fillText("finish_screen", message);
        });
    }

    static show() {
        new ReleaseDialog();
    }
}