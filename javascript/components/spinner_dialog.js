import {Fixture, Template} from "../templates";
import ModalDialog from "../widgets/modal_dialog";


export default class SpinnerDialog extends ModalDialog {
    constructor(id, transparent) {
        super("spinner_dialog", id, true);
        this.template.fillTemplate("spinner", Template.create("spinner"));
        if(transparent) {
            this.template.removeClass("modal--white");
        }
    }

    static show(id, transparent) {
        new SpinnerDialog(id, transparent);
    }

    static hide(id) {
        Fixture.fetch(id).clearTemplate();
    }
}