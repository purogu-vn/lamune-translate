### Run
#### First time
Obtain the latest version of `LamuneTranslate.exe`.
TBD

#### Upgrading
TBD

### Build
#### Install dependencies
Download the latest version of [Python](https://www.python.org/downloads/).
Once installed, run the following in a terminal inside the project folder:

    pip install -r requirements.txt


Install [NodeJS](https://nodejs.org/en/download/).
In the project folder, run the following to install dependencies:

    npm install


#### Configure folder structure
Fill out `lamune.cfg` with valid parameters for your environment (see example config for reference).

#### Development
To build the front end code for development, with automatic building, run:

    node build.js


To start a development server, run:

    python run.py

#### Release
    node build.js --prod