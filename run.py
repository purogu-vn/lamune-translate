import webbrowser
import sys

from python import config
from python.server import Server

PORT = int(config.ServerPort)
if __name__ == '__main__':
	if len(sys.argv) <= 1:
		webbrowser.open_new_tab('http://localhost:{}/index.html'.format(PORT))
	server = Server(PORT)
	print("Ready for traffic")
	server.start()

